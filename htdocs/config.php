<?php

$review_substr_len = 0;
if ($_SERVER['SCRIPT_NAME'] != '/reviews.php') {
    $review_substr_len = 160;
}

$date_start = strtotime('2013-08-30');
$date_end = strtotime('2013-09-15');
$days_left = ceil(($date_end - time()) / 60 / 60 / 24);
$date_open = '15го сентября';

$skillme_email = 'special-box2012@yandex.ru';
$skillme_emails = array(
    'special-box2012@yandex.ru',
    'suvorov.igor.90@gmail.com',
    'skillme@coder24.ru'
);
;
$emails_file = './newcomers.txt';

$percent = round(( time() - $date_start) / ($date_end - $date_start) * 100);

$project_name = 'SkillMe';

//'href' => 'http://www.netcracker.com/rus/',
$reviews = array(
    array(
        'name' => 'Али',
        'img' => '/img/reviews/ali.jpg',
        'href' => 'http://vk.com/barinbritva',
        'review' => //'Познакомился с Игорем, весной 2012, он рассказывал нам про Веб. Веб тенденции, Глубоко обучал фреймворку Yii.',
        '
Во время обучения в институте я самостоятельно изучал web. Я скопил, как мне казалось на тот момент, обширный багаж знаний. Однако, после того как Игорь пригласил меня в свой клуб навыков, я понял, на сколько для развития важен обмен опытом, да и просто общение с людьми, работающими в той же сфере, что и ты. SkillClub – это отличное место для приобретения новых знаний, получения опыта, заведения интересных и полезных знакомств и получения помощи.            
',
        'company_name' => 'Фриланс',
        'company_href' => 'http://aligen.ru',
        'company_post' => 'Веб-программист',
        'weight' => 1,
    ),
    array(
        'name' => 'Александр Бирин',
        'img' => '/img/reviews/birin.jpg',
        'href' => 'http://vk.com/flaeros',
        'review' => '
Проходил курсы по web-разработке у Игоря, 
все рассказывает доступно и понятно, на все вопросы отвечает детально. 
Рассказывает только то что нужно, никакой воды. 
Очень много примеров, после обучения их очень полезно разбирать дома. 
При работе с Игорем всегда есть перспективы развития по разным направлениям. 
',
        'company_name' => 'NetCracker',
        'company_href' => 'http://www.netcracker.com/rus/',
        'company_post' => 'Инженер',
        'weight' => 1,
    ),
    /*
      array(
      'name' => 'Марк Фадеев',
      'img' => '/img/reviews/fadeev.jpg',
      'href' => 'http://vk.com/travelling_man',
      'review' => 'Вместе учились с Игорем, Вместе получили диплом',
      'company_name' => 'NetCracker Technology',
      'company_href' => 'http://www.netcracker.com/rus/',
      'company_post' => 'Старший Программист',
      'weight' => 1,
      ),
      /*
      array(
      'name' => 'Алёна Куренкова',
      'img' => '/img/reviews/kurenkova.jpg',
      'href' => 'http://vk.com/travelling_man',
      'review' => 'в 2012 году, Игорь объяснял мне и некоторым другим ребятам верстку. Я конечно поняла, что это не моё, но объяснял он классно',
      'company_name' => 'Abby',
      'company_href' => 'http://www.netcracker.com/rus/',
      'company_post' => 'Программист',
      'weight' => 1,
      ),
      /*
      array(
      'name' => 'Андрей Кяшкин',
      'img' => '/img/reviews/kyashkin.jpg',
      'href' => 'http://vk.com/travelling_man',
      'review' => 'Знаю Игоря давно, вместе с ним ездили на олимпиады ACM ICPC, организовывал отборочный чемпионаты в нашем ТГУ',
      'company_name' => '33бита',
      'company_href' => 'http://www.netcracker.com/rus/',
      'company_post' => 'Программист',
      'weight' => 1,
      ),

      array(
      'name' => 'Татьяна Лесных',
      'img' => '/img/reviews/lesnyh.jpg',
      'href' => 'http://vk.com/travelling_man',
      'review' => 'Училась с Игорем со школы, потом в Университе. и еще чтото хорошее',
      'company_name' => 'ООО не Тольятти',
      'company_href' => 'http://www.netcracker.com/rus/',
      'company_post' => 'Программист',
      'weight' => 1,
      ), */
    array(
        'name' => 'Хабирова Татьяна',
        'img' => '/img/reviews/habirova.jpg',
        'href' => 'http://vk.com/travelling_man',
        'review' => '
SkillClub - мероприятие, проводимое с целью улучшения (приобретения) навыков web-программирования. 
Около полутора года назад, когда пришла на первое занятие, я знала очень-очень мало об этой области, опыта работы не было совсем. 
Предмет оказался довольно занимательным, особенно учитывая стиль Игоря - главного тренера - объяснять материал: 
ничего лишнего, лишь то, что может пригодиться на практике и легко умещается в голове =) 
Быстро и доходчиво Игорь донёс до нас основные принципы программирования на PHP, основы HTML, JavaScript, взаимодействие этих трёх языков. 
Здесь я узнала, что такое CMS. 
Главное, что мне понравилось - тут же предоставляется возможность использовать полученные навыки на практике, в реальных заказах, что очень важно, ибо практика - это наше всё =) 
Помимо неё, свои знания я подкрепила ещё и проведя пару занятий для новичков Скилл Клаба, нового поколения. 
Осталось море положительных эмоций, знакомств (до сих пор обмениваемся опытом с другими участниками). 
Комфортная, дружеская обстановка, иногда чаепитие (ведь мы вольны проводить занятия в любом формате) - за всё спасибо Скилл Клабу.            
',
        'company_name' => 'NetCracker',
        'company_href' => 'http://www.netcracker.com/rus/',
        'company_post' => '<abbr title="Инженер по автоматизации тестирования">Test Automation Engineer</abbr>',
        'weight' => 2,
    ),
    /*
      array(
      'name' => 'Александр Панин',
      'img' => '/img/reviews/panin.jpg',
      'href' => 'http://vk.com/travelling_man',
      'review' => 'Игорь у меня обучался, потом писал диплом.',
      'company_name' => 'Яндекс',
      'company_href' => 'http://www.netcracker.com/rus/',
      'company_post' => 'Тестировщик',
      'weight' => 3,
      ),
     */
    array(
        'name' => 'Шевченко Екатерина',
        'img' => '/img/reviews/shevchenko.jpg',
        'href' => 'http://vk.com/travelling_man',
        'review' => 'Год назад я ничего не знала о web\'е: ни верстку, ни php, ни javascript.
Несмотря на это, уже через 2 месяца я создала полноценный сайт и закрыла свой первый проект.
И все это благодаря Игорю. Он отлично смог выделить и донести все те знания, которые необходимы web-разработчику.
Он всегда готов был дать ответ на мой даже самый дурацкий вопрос. Он никогда не отказывал в помощи. 
Игорь знает свое дело и он просто отличный человек. У него всегда есть чему поучиться)',
        'company_name' => 'JoyWin',
        'company_href' => 'http://joywin.ru',
        'company_post' => 'Ведущий программист',
        'weight' => 2,
    ),
    /*
      array(
      'name' => 'Кондауров Игорь',
      'img' => '/img/reviews/kondaurov.jpg',
      'href' => 'http://vk.com/travelling_man',
      'review' => 'Познакомились с игорем полтора года назад, сначала фрилансил. Сейчас он технический директор нашей вебстудии. Боец Растет',
      'company_name' => 'JoyWin',
      'company_href' => 'http://joywin.ru',
      'company_post' => 'Генеральный Директор',
      'weight' => 3,
      ),
     */
    array(
        'name' => 'Ксения Босак',
        'img' => '/img/reviews/bosak.jpg',
        'href' => 'http://vk.com/travelling_man',
        'review' => 'Игорь - человек позитивный, компетентный, с горящими глазами и желанием делиться с людьми всем, что он знает! А знает немало. Так что если у вас есть желание учиться и развиваться, не проходите мимо такой уникальной возможности!',
        'company_name' => 'ВебЛайм',
        'company_href' => 'http://weblime.ru',
        'company_post' => 'Дизайнер',
        'weight' => 1,
    ),
    /*
      array(
      'name' => 'Елена Кибанова',
      'img' => '/img/reviews/kibanova.jpg',
      'href' => 'http://vk.com/travelling_man',
      'review' => 'Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.',
      'company_name' => 'Lenka',
      'company_href' => 'http://weblime.ru',
      'company_post' => 'Фрилансер',
      'weight' => 0,
      ),
     */

    /*
      array(
      'name' => 'Елена Кузичкина',
      'img' => '/img/reviews/kuzichkina.jpg',
      'href' => 'http://vk.com/dumperize',
      'review' => 'Игорь хороший, начинали с ним программировать. Объяснял мне jQuery. Помню давно я говорила ему что никогда этому не научусь , и вот прошло 3 года и я тут главная',
      'company_name' => 'Презент',
      'company_href' => 'http://prsnt.ru',
      'company_post' => 'Ведущий программист',
      'weight' => 0,
      ),
     */
    array(
        'name' => 'Глеб Шендрик',
        'img' => '/img/reviews/shendrik.jpg',
        'href' => 'http://vk.com/glebsh',
        'review' => '
Мои впечатления: мастер-класс понравился, для себя узнал новое, считаю что было полезным. =)            
',
        'company_name' => 'Фриланс',
        'company_href' => 'http://prsnt.ru',
        'company_post' => 'Веб-программист',
        'weight' => 0,
    ),
    /*
      array(
      'name' => 'Константин Жуков',
      'img' => '/img/reviews/zhukov.jpg',
      'href' => 'http://vk.com/travelling_man',
      'review' => 'Был у Игоря, понравилось как он объясняет',
      'company_name' => 'Неизвестно',
      'company_href' => 'http://www.netcracker.com/rus/',
      'company_post' => 'Программист',
      'weight' => 1,
      ),
     */
    array(
        'name' => 'Евгений Журавлев',
        'img' => '/img/reviews/zhuravlev.jpg',
        'href' => 'http://vk.com/id5785495',
        'review' => 'Учился с Игорем с первого курса. Формально знаю его 6 лет, однако по настоящему узнал вот только, когда мы начали делать наш небольшой стартап. И как часто бывает первый опыт - горький. Однако ни он, ни я не разочаровались в программировании и мы пытаемся сделать мир лучше.',
        'company_name' => 'T-Systems',
        'company_href' => 'http://www.t-systems.ru/',
        'company_post' => 'Старший инженер-программист',
        'weight' => 1,
    ),
    array(
        'name' => 'Павел Катунин',
        'img' => '/img/reviews/katunin.jpg',
        'href' => 'http://katuninpavel.com',
        'review' => '
Игорь живет новыми идеями и информационными технологиями. 
Креативный, целеустремленный, очень терпеливо преподает. ', /*
          <!--Готов помочь в любую минуту

          учился с игрем с первого курса, но по настоящему узнал вот только когда начали делать наш стартап
          -->
          ', */
        'company_name' => 'Лаборатория Касперского',
        'company_href' => 'http://www.netcracker.com/rus/',
        'company_post' => 'Разрабочик',
        'weight' => 0,
    ),
);
$review_chunks2 = array_chunk($reviews, 2);
shuffle($reviews);
$review_chunks = array_chunk($reviews, 3);

$counters = '';

//////

$accounts = array(
    /* array(
      'title' => 'Система',
      'url' => 'http://boost.ru/tests/'
      ),
      array(
      'title' => 'Артур',
      'url' => 'http://artur.hahabr.ru/'
      ), */
    array(
        'title' => 'Влада',
        'url' => 'http://vlada.hahabr.ru/'
    ),
        /*    array(
          'title' => 'Таня',
          'url' => 'http://tatti.hahabr.ru/'
          ), */
);
$tests = array(
    array(
        'title' => 'Boost1',
        'url_prefix' => 'str1/',
        'cases' => array(
            array(
                'get' => array(
                    'url' => 'http://echo.msk.ru/sounds/1142036.html',
                ),
                'out' => '1142036.html',
            ),
            array(
                'get' => array(
                    'url' => 'http://ru.wikipedia.org/w/index.php?title=%D0%92%D0%B8%D0%BA%D0%B8%D0%BF%D0%B5%D0%B4%D0%B8%D1%8F:%D0%A1%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D1%8F_%D0%BE%D0%B1_%D0%BE%D1%88%D0%B8%D0%B1%D0%BA%D0%B0%D1%85&action=history',
                ),
                'out' => 'index.php',
            ),
            array(
                'get' => array(
                    'url' => 'http://yandex.ru/yandsearch?lr=240&text=%D0%B7%D0%B0%D0%B9%D1%87%D0%B8%D0%BA%D0%B8',
                ),
                'out' => 'yandsearch',
            ),
            array(
                'get' => array(
                    'url' => 'http://skillweb.ru/wp-admin/post.php?post=308&action=edit&message=1',
                ),
                'out' => 'post.php',
            ),
            array(
                'get' => array(
                    'url' => 'http://skillweb.ru/wp-admin/post.php',
                ),
                'out' => 'post.php',
            ),
            array(
                'get' => array(
                    'url' => 'post.php?post=308&action=edit&message=1',
                ),
                'out' => 'post.php',
            ),
            array(
                'get' => array(
                    'url' => 'post.php',
                ),
                'out' => 'post.php',
            ),
        )
    ),
    array(
        'title' => 'Boost2',
        'url_prefix' => '2/',
        'cases' => array(
            array(
                'get' => array(
                    'url' => 'http://echo.msk.ru/sounds/1142036.html',
                ),
                'out' => '1142036.html',
            ),
            array(
                'get' => array(
                    'url' => 'http://ru.wikipedia.org/w/index.php?title=%D0%92%D0%B8%D0%BA%D0%B8%D0%BF%D0%B5%D0%B4%D0%B8%D1%8F:%D0%A1%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D1%8F_%D0%BE%D0%B1_%D0%BE%D1%88%D0%B8%D0%B1%D0%BA%D0%B0%D1%85&action=history',
                ),
                'out' => 'index.php',
            ),
            array(
                'get' => array(
                    'url' => 'http://yandex.ru/yandsearch?lr=240&text=%D0%B7%D0%B0%D0%B9%D1%87%D0%B8%D0%BA%D0%B8',
                ),
                'out' => 'yandsearch',
            ),
            array(
                'get' => array(
                    'url' => 'http://skillweb.ru/wp-admin/post.php?post=308&action=edit&message=1',
                ),
                'out' => 'post.php',
            ),
            array(
                'get' => array(
                    'url' => 'http://skillweb.ru/wp-admin/post.php',
                ),
                'out' => 'post.php',
            ),
            array(
                'get' => array(
                    'url' => 'post.php?post=308&action=edit&message=1',
                ),
                'out' => 'post.php',
            ),
            array(
                'get' => array(
                    'url' => 'post.php',
                ),
                'out' => 'post.php',
            ),
        )
    ),
);

/**
 * Склонение числительныхфывфывфывф
 * @param int $numberof — склоняемое число
 * @param string $value — первая часть слова (можно назвать корнем)
 * @param array $suffix — массив возможных окончаний слов
 * @return string
 *
 */
function numberof($numberof, $value, $suffix) {
    // не будем склонять отрицательные числа
    $numberof = abs($numberof);
    $keys = array(2, 0, 1, 1, 1, 2);
    $mod = $numberof % 100;
    $suffix_key = $mod > 4 && $mod < 20 ? 2 : $keys[min($mod % 10, 5)];

    return $value . $suffix[$suffix_key];
}

function get_users_dir() {
    return __DIR__ . '/signup/users';
}

function get_users($subfolder = '') {
    $files = scandir(get_users_dir());
    if ($subfolder)
        $subfolder .='/';
    $users = array();
    foreach ($files as $filename) {
        if (substr($filename, -5) != '.json')
            continue;
        $user = get_user($subfolder . $filename);
        $user['id'] = $filename;
        $users[] = $user;
    }
    return $users;
}

function get_user($filename) {
    $filename = get_users_dir() . '/' . $filename;
    $content = file_get_contents($filename);
    if (!$content)
        return null;
    $user = json_decode($content, 1);
    return $user;
}

function save_user($user) {
    $filename = get_users_dir() . '/' . date('Y_m_d_H_i_s') . '.json';
    file_put_contents($filename, json_encode($user));
}