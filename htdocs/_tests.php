<?php
require_once 'config.php';

class Account {

    public $data = '';

    public function getTitle() {
        return $this->data['title'];
    }

    public function getUrl() {
        return $this->data['url'];
    }

    public function getEtalonUrl() {
        return 'http://boost.ru/tests/';
    }

    public function doTest($id) {
        global $tests;
        $test = $tests[$id];
        foreach ($test['cases'] as $case) {
            
        }
        return $result;
        return $this->data['title'];
    }

    public function getTestPercentResult($id) {
        $results = $this->getTestResult($id);
        return round(100 * array_sum($results) / count($results));
    }

    public function getTestResult($id) {
        global $tests;
        $results = array();
        foreach ($tests[$id]['cases'] as $case_id => $case) {
            $results[$case_id] = $this->getTestCaseResult($id, $case_id);
        }
        return $results;
    }

    /**
     *
     * @param type $test_id
     * @param type $case_id
     * @return bool 
     */
    public function getTestCaseResult($test_id, $case_id) {
        global $tests;

        $param = http_build_query($tests[$test_id]['cases'][$case_id]['get']);
        if ($param) {
            $param = '?' . $param;
        }
        $etalon_url = $this->getEtalonUrl() . $tests[$test_id]['url_prefix'] . $param;
        $etalon_result = @file_get_contents($etalon_url);
        // $case = 
        $case_url = $this->getUrl() . $tests[$test_id]['url_prefix'] . $param;
        $case_result = @file_get_contents($case_url);



        return $case_result == $etalon_result;
        //return file_get_contents($etalon_url) == file_get_contents($url);
    }

}

class BoostTester {

    public static function getAccounts() {
        global $accounts;
        $accounts2 = array();
        foreach ($accounts as &$account_data) {
            $account = new Account();
            $account->data = $account_data;
            $accounts2[] = $account;
        }
        return $accounts2;
    }

    public static function getTestsResult() {
        global $accounts;
        global $tests;
        foreach ($accounts as &$account) {
            foreach ($tests as $test_key => $test) {
                $account['tests'] = '';
            }
        }
        return $accounts;
    }

}
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Template &middot; Bootstrap</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 20px;
                padding-bottom: 40px;
            }

            /* Custom container */
            .container-narrow {
                margin: 0 auto;
                max-width: 700px;
            }
            .container-narrow > hr {
                margin: 30px 0;
            }

            /* Main marketing message and sign up button */
            .jumbotron {
                margin: 60px 0;
                text-align: center;
            }
            .jumbotron h1 {
                font-size: 72px;
                line-height: 1;
            }
            .jumbotron .btn {
                font-size: 21px;
                padding: 14px 24px;
            }

            /* Supporting marketing content */
            .marketing {
                margin: 60px 0;
            }
            .marketing p + h4 {
                margin-top: 28px;
            }
        </style>
        <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://getbootstrap.com/2.3.2/assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://getbootstrap.com/2.3.2/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://getbootstrap.com/2.3.2/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://getbootstrap.com/2.3.2/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="http://getbootstrap.com/2.3.2/assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="http://getbootstrap.com/2.3.2/assets/ico/favicon.png">
    </head>

    <body>

        <div class="container-narrow">

            <div class="masthead">
                <ul class="nav nav-pills pull-right">
                    <li class="active"><a href="#">Главная</a></li>
                    <li><a href="#scores">Рейтинг</a></li>
                    <li><a href="#contacts">Контакты</a></li>
                </ul>
                <h3 class="muted">SkillWeb <sup><small><span class="label label-warning">BETA</span></small></sup> </h3> 
            </div>

      
            <h3>Рейтинг</h3>
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Имя</th>
                    <?php
                    foreach ($tests as $test_key => $test) {
                        ?>
                        <th>
                            <?php echo $test['title']; ?>
                        </th>
                        <?php
                    }
                    ?>
                </tr>
                <?php
                foreach (BoostTester::getAccounts() as $account) {
                    ?>
                    <tr>
                        <th>
                            <?php echo $account->getTitle(); ?>
                        </th>
                        <?php
                        foreach ($tests as $test_key => $test) {
                            ?>
                            <td>
                                <?php echo $account->getTestPercentResult($test_key); ?>%
                            </td>
                            <?php
                        }
                        ?>
                    </tr>

                    <?php
                }
                ?>
            </table>

            <div class="footer">
                <p>&copy; <a href="http://skillclub.ru">SkillClub</a> 2013</p>
            </div>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="http://getbootstrap.com/2.3.2/assets/js/jquery.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-transition.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-alert.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-modal.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-dropdown.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-scrollspy.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-tab.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-tooltip.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-popover.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-button.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-collapse.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-carousel.js"></script>
        <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-typeahead.js"></script>

        <div id="signup" class="modal hide fade in" style="display: none; ">
            <div class="modal-header">
                <a class="close" data-dismiss="modal">×</a>
                <h3>Зарегистрироваться</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <fieldset>


                        <!-- Prepended text-->
                        <div class="control-group">
                            <label class="control-label">Ваше Имя</label>
                            <div class="controls">
                                <div class="input">
                                    <input name="signup[name]" id="prependedtext" name="prependedtext" class="span3" placeholder="placeholder" type="text">
                                </div>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Ваш Email</label>
                            <div class="controls">
                                <div class="input">
                                    <input name="signup[email]" id="prependedtext" name="prependedtext" class="span3" placeholder="placeholder" type="text">
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-success" onclick="signup();">Хочу, все знать!</a>
                <a href="#" class="btn" data-dismiss="modal">Нет, спасибо</a>
            </div>
        </div>

    </body>
</html>
