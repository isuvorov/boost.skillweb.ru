<?php require dirname(__DIR__) . '/config.php'; ?>
<?php require_once dirname(__DIR__) . '/_header.php'; ?>

<style>
    .pre textarea{
        width:100%;
    }
    #question_groups section{
        /*///*/display: none;
    }
    #prev_step{
        display: none;
    }
    #next_step{
        display: none;
    }
    textarea{
        width:100%;
        height:130px;
    }
    input{
        width:50%;

    }
    .label{
        padding: 5px 20px;
        margin:5px;
        font-size: 16px;
    }
    <?php
    if (!$is_preview) {
        ?>
        #question_group_tag textarea
        {
            display:none;
        }
        <?php
    }
    ?>
</style>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1.4.3");
    var step = 0;
    var last_step = 12;
    google.setOnLoadCallback(function() {
        $(function() {
            $('#question_group_tag .label').click(function() {
                $(this).toggleClass('label-success');
                fill_tag_textarea();
//                if ($(this).hasClass('label-success'))
//                    return false;
//                var val = $('#question_group_tag textarea').val();
//                if (val) {
//                    val += ', ';
//                }
//                val += $(this).html();
//                $('#question_group_tag textarea').val(val);
//                $(this).addClass('label-success');
            });
            $('#question_group_tag .label').css('cursor', 'pointer');
            /*$('#question_group_tag .label').css('font-size', '18px');
             $('#question_group_tag .label').css('line-height', '18px');
             $('#question_group_tag .label').css('margin', '10px;');
             */
            //
            show();
        });
    });

    function fill_tag_textarea() {
        var val = '';
        $('#question_group_tag .label-success').each(function() {
            if (val) {
                val += ', ';
            }
            val += $(this).html();
        });
        $('#question_group_tag textarea').val(val);
    }
    function next_step() {
        step += 1;
        show();
    }
    function prev_step() {
        step -= 1;
        if (step > last_step) {
            step = last_step;
        }
        show();
    }
    function show() {
        /*///*/$('#question_groups section').hide();
        $('#question_groups section:nth-child(' + (step + 1) + ')').show();

        if (step === last_step) {
            $('#next_step').hide();
        } else {
            $('#next_step').show();
        }

        if (step === 0) {
            $('#prev_step').hide();
        } else {
            $('#prev_step').show();
        }

        progress = 100 * (step + 1) / (last_step + 1);
        //console.log(progress);

        $('#progress').css("width", progress + "%");

<?php
if ($is_preview) {
    ?>
            $('#question_groups section').show();
    <?php
}
?>
    }
</script>
<?php
if ($is_preview && $_GET['id']) {
    $user = get_user($_GET['id']);
}

function question($name, $type = 'textarea', $value = '') {
    global $user;
    if (!$value && $user) {
        $value = $user[$name];
    }
    if ($type == 'textarea') {
        ?>
        <div>
            <textarea  name="signup[<?php echo $name; ?>]"><?php echo $value; ?></textarea>
        </div>
        <?php
    } else {
        ?>
        <div>
            <input type="text"  name="signup[<?php echo $name; ?>]" value="<?php echo $value; ?>">
        </div>
        <?php
    }

    //echo $num;
}
?>

<!--
<div class="jumbotron">
    <a class="btn btn-large btn-success" href="#write_review" data-toggle="modal">Тоже хочу написать отзыв</a>
</div>
<div class="page-header">
    <h1>Анкета учатника</h1>
</div>
-->
<section id="question_groups">
    <form action="save.php" method="POST">
        <section>
            <div class="jumbotron">
                <h2 style="margin-bottom:0px;">Хочешь научиться </h2>
                <h1 style="margin-top:0px;margin-bottom:0px;font-size: 70px;">ПРОГРАММИРОВАТЬ?</h1>
                <p class="lead" style="margin-top:0px;">Заполни анкету, чтобы мы знали, что тебе предложить</p>
            </div>
        </section>    
        <section>
            <h2>Общие данные</h2>
            <br>
            <h4>ФИО</h4>
            <?php question('fio', 'input', $_GET['name']); ?>
            <br>
            <h4>ВУЗ, Группа, Специальность</h4>
            <?php question('spec', 'input'); ?>
            <br>
            <h4>Возраст</h4>
            <?php question('age', 'input'); ?>
            <br>
            <h4>Email</h4>
            <?php question('email', 'input', $_GET['email']); ?>
            <br>
            <h4>Телефон</h4>
            <?php question('phone', 'input'); ?>
            <br>
            <h4>Страница VK (facebook, skype …)</h4>
            <?php question('social', 'textarea'); ?>
        </section>    
        <section>
            <h2>Профессия</h2>
            <br>
            <h4>С чем связан выбор Вашей профессии?</h4>
            <?php question(7); ?>
            <br>
            <h4>По какому принципу Вы выбирали ВУЗ и специальность?</h4>
            <?php question(8); ?>
        </section>  
        <section>
            <h4>Вы студент</h4>
            <p>На экзамене Вы получаете средненькую оценку.
                Преподаватель предлагает Вам взять еще один билет.
				С равной вероятностью Вам может попасться билет, который Вы знаете или нет.
                Если ответите на него правильно - Вы получите оценку выше, если нет - оценку ниже.
            <p><b>Cогласитесь Вы или откажетесь? И почему?</b></p>
            <?php question(9); ?>
        </section>  
        <section>
            <h4>Какие языки программирования Вы знаете? И насколько глубоко?</h4>
            <?php question(10); ?>
        </section>  
        <section id="question_group_tag">
            <h4>Какие из этих слов Вам знакомы?</h4>
            <div class="alert alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>

                <p><b>Подсказка:</b> Теги являются кликабельными</p>
            </div>
            <?php include(__DIR__.'/_tags.php'); ?>
            <?php question(11); ?>
        </section>  
        <section>
            <h4>Кто есть кто</h4>
            <p>
                Представьте, что Вы пришли устраиваться в маленькую, но подающую большие надежды компанию. 
                Директор решает познакомить вас со своей командой. 
                Он зовет троих сотрудников: 
            </p>
            <ul>
                <li>Дизайнера</li>
                <li>Программиста</li>
                <li>Админа</li>
            </ul>
            <p>
                Первый заходит и с порога бодро заявляет: "Я дизайнер". 
                Следом второй, таинственно улыбаясь: "Я не дизайнер". 
                Через минуту входит третий, усталый на вид и, покачивая головой, отнекивается: "Я не программист".
            </p>
            <p>
                Директор, откидываясь на спинку кресла и растягиваясь в хитрой улыбке, замечает: "Только один правду сказал!  
            </p>
            <p><b>
                    Как догадаться кто из них кто?
                    Опишите ход ваших мыслей?
                </b></p>

            <?php question(12); ?>
        </section>  
        <section>
            <h4>Какие качества в людях Вас вдохновляют? <br> Кого Вы считаете успешными? <br> Назовите примеры таких личностей </h4>
            <?php question(13); ?>
        </section>  
        <section>
            <h4>Строите ли Вы планы на год?<br>  Каким Вы представляете свое будущее?  </h4>
            <?php question(14); ?>
        </section>  
        <section>
            <h4>Чем Вы любите заниматься в свободное время?</h4>
            <?php question(15); ?>
        </section>  
        <section>
            <h4>Билл Гейтс может выполнить любое Ваше желание. </h4>
            <p>Вы можете сделать или изменить одну вещь на планете? Вещь в сфере IT. Желание всего одно!</p>
            <p><b>Что Вы выберете?</b></p>

            <?php question(16); ?>
        </section>  
        <section>
            <h4>Что Вы ожидаете от курса и какие надежды возлагаете на него?</h4>	 
            <?php question(17); ?>
        </section>  
        <section>
            <div class="jumbotron">
                <h2 style="margin-bottom:0px;">Отлично! Вы дошли до конца!</h2>
                <p class="lead" style="margin-top:0px;">На этом вопросы закончились, нажмите на кнопку отправить. Мы напишем Вам в скором времени.</p>
                <input type="submit" class="btn btn-large btn-success" value="Отправить!" />
            </div>
        </section>  
    </form>
</section>
<table width="100%">
    <tr>
        <td style="width:200px;text-align:center;">
            <a href="#"  class="btn btn-large" onclick="prev_step();" id="prev_step">&larr; Назад</a>
        </td>
        <td style="text-align:center;padding-top:20px;">
            <div class="progress progress-striped " style="height:34px;">
                <div class="bar" style="width: 00%;" id="progress"></div>
            </div>
        </td>
        <td style="width:200px;text-align:center;">
            <a href="#"  class="btn btn-large btn-success" onclick="next_step();" id="next_step">Дальше &rarr;</a>
        </td>
    </tr>
</table>

<?php require_once dirname(__DIR__) . '/_footer.php'; ?>