<?php
$file = file_get_contents(__DIR__ . '/tags.txt');
$tags = explode("\n", $file);
$tag_groups = array();
$tag_group_count = 0;
foreach ($tags as $tag) {
    $tag = trim($tag);
    if (!$tag) {
        if (count($tag_groups[$tag_group_count]))
            $tag_group_count += 1;
        continue;
    }
    $tag_groups[$tag_group_count][] = $tag;
}


foreach (array_slice($tag_groups, 0, 5) as $tags) {
    shuffle($tags);
    foreach ($tags as $tag) {
        ?>
        <span class="label"><?php echo $tag; ?></span>
        <?php
    }
    echo '<span> &nbsp; </span>';
}
?>
<script>
    function show_tags() {
        $('#hidden_tags').show();
        $('#show_tags').hide();
        $('#show_tags').css('cursor','pointer');
    }
</script>
<a onclick="show_tags();" id="show_tags" class="btn">
    Показать еще теги
</a>
<span id="hidden_tags" style="display:none;">
    <?php
    foreach (array_slice($tag_groups, 5) as $tags) {
        shuffle($tags);
        foreach ($tags as $tag) {
            ?>
            <span class="label"><?php echo $tag; ?></span>
            <?php
        }
        echo '<span> &nbsp; </span>';
    }
    ?>
</span>