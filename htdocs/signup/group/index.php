<?php require dirname(dirname(__DIR__)) . '/config.php'; ?>
<?php require_once dirname(dirname(__DIR__)) . '/_header.php'; ?>
<h1>Список группы</h1>
<?php
$users = get_users();
?>
<table class="table table-striped table-bordered">
    <tr>
        <th>ФИО
        <th>Группа
    </tr>
    <?php
    foreach ($users as $user) {
        ?>
        <tr>
            <td><?php echo $user['fio']; ?></td>
            <td><?php echo $user['spec']; ?></td>
            <?php
            if ($is_preview) {
                ?>
                <td>
                    <?php echo $user['phone']; ?><br>
                    <a href="?id=<?php echo $user['id']; ?>">[подробнее]</a><br>
                    <a href="<?php echo $user['social']; ?>">[<?php echo $user['social']; ?>]</a>
                    
                    
                    </td>
                <td>
                    <?php var_dump($user) ; ?>
                    <?php
                }
                ?>
        </tr>
        <?php
    }
    ?>
</table>
<?php require_once dirname(dirname(__DIR__)) . '/_footer.php'; ?>