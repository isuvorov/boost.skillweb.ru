<?php require 'config.php'; ?>
<?php require_once './_header.php'; ?>


<div class="jumbotron">
    <h2 style="margin-bottom:0px;">Хочешь научиться </h2>
    <h1 style="margin-top:0px;margin-bottom:0px;font-size: 70px;">ПРОГРАММИРОВАТЬ?</h1>
    <p class="lead" style="margin-top:0px;">Запишись и скоро мы расскажем тебе как</p>
    <a class="btn btn-large btn-success" href="#signup" data-toggle="modal">Хочу!</a>
</div>

<div class="jumbotron">
    <h2>Все очень просто</h2>
    <p class="lead">От тебя требуется только <a  href="#signup" data-toggle="modal">записаться в добровольцы</a> <br>и прийти сюда снова 15 сентября</p>
    <p class="lead">А мы в свою очередь...</p>
</div>

<div class="row-fluid how_block">
    <div class="span4">
        <img src="/img/img5.jpg" class="img-circle" width="180" height="180">
        <h2>Научим</h2>
        <p>
            Мы подготовили пошаговую инструкцию. Разбили наш большой курс на <abbr title="Азы языка, Работа с файлами, Передача данных, Проблемы кодировок...">небольшие кусочки</abbr>. 
            Мы постарались собрать интересный, яркий - материал, объясняющий, как решать подобные задачу: Статьи, Лекции, Видеоуроки, Мастерклассы.
        </p>

    </div>
    <div class="span4">

        <img src="/img/img3.jpg" class="img-circle" width="180" height="180">
        <h2>Проверим</h2>
        <p>
            Теория - важна. Но практика важнее. Именно поэтому мы сконцентрировались на этой части. Вы делаете различные задания, система вас проверяет. Она быстра, хитра и непредвзята :)
            Наш робот в режиме онлайн даёт подсказки и указывает на узкие места решения.
        </p>
    </div>
    <div class="span4">

        <img src="/img/img2.jpg" class="img-circle" width="180" height="180">
        <h2>Поможем</h2>
        <p>
            Когда вы самостоятельно решаете задачу, очень часто возникают затруднения - порой это напоминает <abbr title="Конечно очень интересно, пройти его самостоятельно. Но иногда, с точки зрения продуктивности, просто необходима помощь профессионала">квест</abbr>. Именно в этот момент мы подскажем как лучше решить те или иные задачи. 
            Укажем на альтернативные варианты решения проблемы. 
        </p>

    </div>
</div>


<?php
/*
<hr/>
<div class="jumbotron without_margin">
    <h2>Наша система</h2>
</div>
<?php require_once('_system.php'); ?>

<?php
/*
<div class="row-fluid marketing">
    <div class="span6">
        <h4>Теория</h4>
        <p>Теория это очень важно</p>

        <h4>Практика</h4>
        <p>Теория это очень важно, но практика важнее. Именно поэтому, мы создали эту систему</p>

        <h4>Подсказки (мы поможем), бонусы</h4>
        <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
    </div>

    <div class="span6">
        <h4>Автоматическая система</h4>
        <p>Все задания первоначально проверяет наш интеллектуальный робот. Благодаря этому, ты уже через несколько секунд получить результат выполнения работы</p>

        <h4>Человек</h4>
        <p>Мы очень любим нашего робота, но заключительную точку ставит человек</p>

        <h4>Beta</h4>
        <p>Система пока находится в стадии тестирования. Ну и  как всегда будет не без заминок :-)</p>
    </div>
</div>

<hr/>
<div class="row-fluid">
    <div class="jumbotron without_margin">
        <h1>Прими участие</h1>
        <p class="lead"><!--Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.--></p>
        <a class="btn btn-large btn-success" href="#signup" data-toggle="modal">Хочу все знать!</a>
    </div>
</div>
*/ ?>
<hr/>
<div class="row-fluid">
    <div class="jumbotron without_margin">
        <h1>Кто мы?</h1>
    </div>
</div>
<style>
    .whoami p{
        line-height:20px;
        text-indent: 15px;
        text-align: justify;
    }
    .whoami .image{
        padding-top:30px;
    }
</style>

<div class="row-fluid marketing whoami">
    
    <div class="span2 image" >
        <img src="/img/suvorov_3.jpg" class="img-circle" width="140" height="140">
    </div>
    <div class="span4">
        <h4>Игорь Суворов</h4>

        <div class="row-fluid">
            
            
            <p>
                Меня зовут Игорь Суворов. Мне 23. 
                Программист-предприниматель. 
                Работаю в области веб-технологий с 1го курса университета, вот уже 6 лет.  
                Технический директор и глава Тольяттинского филиала вебстудии JoyWin.
                Старший преподаватель в <abbr titile="Тольяттинский Государственный Университет">ТГУ</abbr>.
                
                <br><b>Мне не все равно</b>, что происходит в нашей IT области, поэтому 1.5 года назад я создал проект SkillClub.
            </p>

        </div>
    </div>
    <div class="span2 image" >
        <a href="http://skillclub.ru"><img src="/img/skillclub.jpg" width = "140" height ="140" class="img-circle" alt="SkillClub" title="SkillClub - сообщество проактивного обучения" style="float:left"/></a>
    </div>
    <div class="span4">
        <h4>SkillClub</h4>

        <div class="row-fluid">
            <p>
                SkillClub - мы сообщество людей которым <b>не безразлично</b> наше профессиональное будущее.
                Мы проводим бесплатные тренинги и мастерклассы, для повышения навыков в области IT технологий. 
                За время нашего существования, мы познакомились более чем с 60 талантами, и постарались передать им всё, чем мы владеем.
            </p>

        </div>
    </div>

</div>




<hr/>

<div class="row-fluid">
    <div class="jumbotron without_margin"  style = "margin-bottom: 30px;">
        <h1>Отзывы</h1>
    </div>
</div>

<!--
<div class="span12">
    Мы уже не первый год, готовим программистов, через нашу школу прошли более 30 программистов. Но этот поток будет супер.
</div>
-->
<?php
require('_reviews.php');
?>
<!--<a href="/reviews.php" style="float:right">прочитать все отзывы</a>-->  

<hr/>
<div class="row-fluid">
    <div class="jumbotron">
        <h1>
            <?php
            echo numberof($days_left, 'Остал', array('ся', 'ось', 'ось'));
            echo ' ';
            echo $days_left;
            echo ' ';
            echo numberof($days_left, '', array('день', 'дня', 'дней'));
            ?>
        </h1>
        <div class="progress progress-striped active">

            <div class="bar" style="width: <?php echo $percent; ?>%;"><?php echo $percent; ?>%</div>
        </div>

        <p class="lead">Мы сейчас стремительно завершаем последние приготовления по Проекту. И первая группа стартует уже <?php echo $date_open; ?>. Спешите, ведь места ограничены!</p>
        

        <a class="btn btn-large btn-success" href="#signup" data-toggle="modal">Ура! Я с ВАМИ!</a>
    </div>
</div>

<hr/>

<div class="row-fluid">
    <div class="jumbotron without_margin">
        <h1>Обратная связь</h1>
        <p class="lead">Если у вас остались какие-либо вопросы, пожалуйста задайте их. <br>Мы ответим на <abbr title="кроме очень очень очень глупых">все</abbr> вопросы!</p>
        <p class="lead">P.S. Мне, очень важно Ваше мнение о Проекте, и я бы, ОЧЕНЬ, попросил написать мне пару слов.</p>
        <p class="lead">Пишите в любое время ночи :)</p>
        
        
    </div>
</div>



<div class="row-fluid">
     <div class = "span2 contacts offset2">
        <img src="/img/suvorov_3.jpg" class="img-circle" width="140" height="140">
        <address>
            <strong>Игорь Суворов</strong><br>
            <a href="mailto:me@isuvorov.ru">me@isuvorov.ru</a><br>
            +7 (917) 829-2237<br>
            <a href="skype:igor.suvorov.90?call"  title="Skype">igor.suvorov.90</a><br>
            <a href="http://vk.com/igor.suvorov"  title="Вконтакте Игорь Суворов">Вконтакте</a><br>
        </address>
    </div>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?98"></script>
    <?php/*<div class="span3 offset1">
        
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?98"></script>

        <!-- VK Widget -->
        <div id="vk_groups"></div>   
        <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: '', color2: '', color3: ''}, 35852347);
        </script>
    </div> */ ?>
    <div class="span6">

        <!-- Put this script tag to the <head> of your page -->
        <!--<script type="text/javascript" src="//vk.com/js/api/openapi.js?98"></script>-->

        <script type="text/javascript">
            VK.init({apiId: 3851786, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
            VK.Widgets.Comments("vk_comments", {limit: 10, width: "480", attach: "*"});
        </script>


    </div>
</div>
<!--
<div class="row-fluid">
    <div class = "span2 contacts">
        <img src="/img/suvorov_3.jpg" class="img-circle" width="140" height="140">
        <address>
            <strong>Игорь Суворов</strong><br>
            <a href="mailto:#">me@isuvorov.ru</a><br>
            +7 (917) 829-2237<br>
            <a href="skype:igor.suvorov.90?call"  title="Skype">igor.suvorov.90</a><br>
            <a href="http://vk.com/im?media=&sel=158358029"  title="Skype">vk</a><br>
        </address>
    </div>
    <div class = " offset2 span8">
       
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?98"></script>

        <script type="text/javascript">
            VK.init({apiId: 3852004, onlyWidgets: true});
        </script>

      
        <div id="vk_comments"></div>
        <script type="text/javascript">
            VK.Widgets.Comments("vk_comments", {limit: 10, width: "658", attach: "*"});
        </script>
    </div>
</div>
-->
<div class="jumbotron">
    <a class="btn btn-large btn-success" href="#signup" data-toggle="modal">Я с ВАМИ!</a>
</div>



<?php /* <div class="row-fluid">
  <div class="jumbotron">
  <h1>Отзывы</h1>
  </div>
  </div>

  <div class="row-fluid" id="reviews">
  <?php
  shuffle($reviews);
  foreach (array_slice($reviews, 0, 3) as $review) {
  ?>
  <div class="span4">
  <img src="<?php echo $review['img']; ?>" class="img-rounded" width="140" height="140">

  <p><?php echo $review['review']; ?></p>
  <p class="pull-right" style="text-align:right;">
  <?php echo $review['name']; ?><br>
  <?php echo $review['company_post']; ?>, <?php echo $review['company_name']; ?>
  </p>
  <!--<p><a class="btn" href="#">View details »</a></p>-->
  </div>
  <?php
  }
  ?>
  </div>

  <div class="row-fluid">
  <p><a class="btn" href="#">Прочитать все отзывы»</a></p>
  </div> */ ?>

<?php /*
  <div class="jumbotron">
  <h1>VK комментарии</h1>
  <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
  </div> */ ?>

<!--
            <div class="row-fluid marketing">
                <div class="span6">
                    <h4>Subheading</h4>
                    <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

                    <h4>Subheading</h4>
                    <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>

                    <h4>Subheading</h4>
                    <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
                </div>

                <div class="span6">
                    <h4>Subheading</h4>
                    <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

                    <h4>Subheading</h4>
                    <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>

                    <h4>Subheading</h4>
                    <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
                </div>
            </div>

            <hr>
-->

<?php /* <div class="row-fluid marketing">
  <div class="span6">
  <h4>Автоматическая система</h4>
  <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

  <h4>Subheading</h4>
  <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>

  <h4>Subheading</h4>
  <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
  </div>

  <div class="span6">
  <h4>Subheading</h4>
  <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

  <h4>Subheading</h4>
  <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>

  <h4>Subheading</h4>
  <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
  </div>

  </div>

  <div class="row">
  <div class="span4">
  <h2>Heading</h2>
  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
  <p><a class="btn" href="#">View details »</a></p>
  </div>
  <div class="span4">
  <h2>Heading</h2>
  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
  <p><a class="btn" href="#">View details »</a></p>
  </div>
  <div class="span4">
  <h2>Heading</h2>
  <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
  <p><a class="btn" href="#">View details »</a></p>
  </div>
  </div> */ ?>
<?php require_once './_footer.php'; ?>