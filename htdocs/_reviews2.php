<div class="row-fluid without_margin" id="reviews">
    <?php
    foreach ($review_chunks2[0] as $review) {
        ?>
        <div class="span6">
            <div class = "review_author">
                <div class="author_img pull-left" >
                    <img src="<?php echo $review['img']; ?>" class="img-circle" width="120" height="120">
                </div>

                <p class = "author_info">
                    <span class = "author_name"><?php echo $review['name']; ?></span><br>
                    <span class = "author_prof"><?php echo $review['company_post']; ?>,<br> <?php echo $review['company_name']; ?></span>
                </p>
                <div style ="clear: both;"></div>
            </div>

            <p class ="review_text">
                <?php
                
                if($review_substr_len)
                    $review_text = mb_substr($review['review'], 0, $review_substr_len, "UTF-8");
                else
                    $review_text = $review['review'];
                $review_text_not_full = $review_text != $review['review'];

                echo $review_text;
                ?>

                <?php
                if ($review_text_not_full) {
                    echo '...';
                }
                ?>
            </p>
            <!--<p><a class = "btn" href = "#">View details »</a></p>-->
        </div>
        <?php
    }
    unset($review_chunks2[0]);
    $review_chunks2 = array_values($review_chunks2);
    ?>
    <div style ="clear: both;"></div>
</div>