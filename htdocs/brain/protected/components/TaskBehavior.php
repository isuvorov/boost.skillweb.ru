<?php

class TaskBehavior extends CActiveRecordBehavior {

    public function getUserTask($user_id = null) {
        if ($user_id === null) {
            $user_id = Yii::app()->user->id;
        }
        $params = array(
            'user_id' => $user_id,
            'task_id' => $this->owner->id,
        );
        $user_task = UserTask::model()->findByAttributes($params);
        if ($user_task == null) {
            return $this->createUserTask($user_id);
        } else {
            return $user_task;
        }
    }

    public function createUserTask($user_id) {
        $user_task = new UserTask();
        $user_task->user_id = $user_id;
        $user_task->task_id = $this->owner->id;
        $user_task->started_at = new CDbExpression('NOW()');
        $user_task->updated_at = new CDbExpression('NOW()');
        $user_task->finished_at = null;
        $user_task->info = null;
        $user_task->score = 0;
        $user_task->save();
        //var_dump($user_task->errors);
        //exit;
        return $user_task;
    }

    public function getUsersCount() {
        $params = array(
            'task_id' => $this->owner->id,
        );
        return UserTask::model()->countByAttributes($params);
    }

    public function getUsersFinalCount() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'task_id = :task_id AND finished_at IS NOT NULL';
        $criteria->params = array(
            ':task_id' => $this->owner->id,
        );
        return UserTask::model()->count($criteria);
    }

    public function isStarted() {
        return strtotime($this->owner->started_at) < time();
    }

}
