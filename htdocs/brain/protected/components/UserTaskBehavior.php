<?php

function ping($domain) {
    $ans = exec('ping -n 1 -w 1 ' . $domain);
    ;
//анализ
    echo $ans;
}

class UserTaskBehavior extends CActiveRecordBehavior {

    public $game;

    public function getTask() {
        return Task::model()->findByPk($this->owner->task_id);
    }

    public function getGame() {
        $this->loadGame();
        return $this->game;
    }

    public function loadGame() {
        $this->game = unserialize($this->owner->info);
        if ($this->game == false) {
            $class = $this->getTask()->class;
            $this->game = new $class();
        }
    }

    public function saveGame() {
        $this->owner->info = serialize($this->game);
    }

    /**
     * InSeconds
     */
    public function getPlayDuration() {
        //return strtotime($this->getPlayFinish()) - now();
        return strtotime($this->getPlayFinish()) - strtotime($this->getPlayStart());
    }

    public function getPlayStart() {
        return $this->owner->started_at;
        /* var_dump($this->owner->started_at);
          if ($in_seconds)
          return strtotime($this->owner->started_at);
          else
          return $this->owner->started_at;//date('Y-m-d H:i:s', $this->owner->started_at);; */
    }

    public function isStarted() {
        return $this->owner->started_at && strtotime($this->owner->started_at) < time();
    }

    public function isFinished() {
        return $this->owner->finished_at && $this->owner->finished_at != '0000-00-00 00:00:00';
    }

    public function finish() {
        $this->owner->finished_at = new CDbExpression('NOW()');
        $this->owner->score = $this->game->getScore();
    }

    public function updated() {
        $this->owner->updated_at = new CDbExpression('NOW()');
    }

    public function getPlayFinish() {
        if ($this->isFinished())
            return $this->owner->finished_at;
        else
            return date('Y-m-d H:i:s'); //now();
    }

}
