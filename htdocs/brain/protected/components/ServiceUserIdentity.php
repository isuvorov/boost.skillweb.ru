<?php

class ServiceUserIdentity extends UserIdentity {

    const ERROR_NOT_AUTHENTICATED = 3;

    /**
     * @var EAuthServiceBase the authorization service instance.
     */
    protected $service;
    private $_id;

    /**
     * Constructor.
     * @param EAuthServiceBase $service the authorization service instance.
     */
    public function __construct($service) {
        $this->service = $service;
    }

    /**
     * Authenticates a user based on {@link username}.
     * This method is required by {@link IUserIdentity}.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        if ($this->service->isAuthenticated) {

            $this->username = $this->service->id . '@' . $this->service->serviceName;
            $user = User::model()->find('LOWER(username)=?', array(strtolower($this->username)));

            //$user = User::model()->findByAttributes(array('username' => $this->username));
            if ($user === null) {
                $user = new User();
                $user->username = $this->username;
                $user->email = $this->username;
                $user->profile = serialize(array('name' => $this->service->getAttribute('name')));

                $user->password = $user->hashPassword(md5($this->username));
                $user->save();

                $this->_id = $user->id;
                $this->errorCode = self::ERROR_NONE;
            } else {
                $this->_id = $user->id;
                $this->username = $user->username;
                $this->errorCode = self::ERROR_NONE;
            }
            //$this->setState('id', $this->service->id);
            $this->setState('name', $this->service->getAttribute('name'));
            $this->setState('service', $this->service->serviceName);
            $this->errorCode = self::ERROR_NONE;
        } else {
            $this->errorCode = self::ERROR_NOT_AUTHENTICATED;
        }
        return !$this->errorCode;
    }

}
