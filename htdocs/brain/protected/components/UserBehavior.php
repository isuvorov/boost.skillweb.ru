<?php

class UserBehavior extends CActiveRecordBehavior {

    public function getScore() {
        $score = Yii::app()->db->
                createCommand("SELECT SUM(`score`) as `score` FROM {{user_task}} WHERE user_id = :user_id")->
                bindValue(":user_id", $this->owner->id)->
                queryScalar();
        //$this->owner->score = $score; //save
        // UserBehavior::model()->findBySql('SELECT SUM(score) FROM {{user_task}} WHERE 1')
        return $score ? $score : 0;
    }

    public function getTasksCount() {
        $params = array(
            'user_id' => $this->owner->id,
        );
        return UserTask::model()->countByAttributes($params);
    }

    public function getFinishedTasksCount() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'user_id = :user_id AND finished_at IS NOT NULL';
        $criteria->params = array(
            ':user_id' => $this->owner->id,
        );
        return UserTask::model()->count($criteria);
    }

    public function getProfile() {
        return unserialize($this->owner->profile);
    }

    public function getName() {
        $profile = $this->getProfile();
        if ($profile) {
            $name = $profile['name'];
        }
        return $name ? $name : $this->owner->username;
    }

    public function getProfileLink() {
        return $this->getExternalLink() ? $this->getExternalLink() : '/brain/site/user/id/' . $this->owner->id;
    }

    public function getExternalLink() {
        if (strpos($this->owner->username, '@')) {
            return 'http://vk.com/id' . strstr($this->owner->username, '@', true);
        }
        return null;
    }

    public function isAdmin() {
        return $this->owner->id == 1;
    }

    public function isTester() {
        return $this->owner->id < 10;
    }

    public function beforeSave($event) {
        if ($this->owner->isNewRecord) {
            $this->owner->created_at = new CDbExpression('NOW()');
        }
        //$this->owner->score = $this->owner->getFinishedTasksCount();
        return parent::beforeSave($event);
    }

}
