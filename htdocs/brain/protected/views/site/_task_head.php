<?php
//$name = $task->name;
$name = $this->name;
$this->pageTitle = Yii::app()->name . ' - ' . $name;
$this->breadcrumbs = array(
    'Задачи' => $this->createAbsoluteUrl('site/tasks'),
    $name,
);
?>
<h1><?php echo $name; ?></h1>