<?php $this->renderPartial('_task_head', $params); ?>
<?php
echo $game->getOutro();
?>
<br />
<center>
    <a href="<?php echo $this->createAbsoluteUrl('site/tasks'); ?>" class="btn btn-success btn-large">Просмотреть еще задачи</a>
    или <a href="<?php echo $this->createAbsoluteUrl('site/users'); ?>" class="btn btn-success btn-large">Познакомится с пользователями</a>
</center>
<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?105"></script>

<script type="text/javascript">
<?php
if ($_SERVER['HTTP_HOST'] == 'skillme.office.skillweb.ru') {
    ?>
        VK.init({apiId: 4027289, onlyWidgets: true});
    <?php
} else {
    ?>
        VK.init({apiId: 3851786, onlyWidgets: true});
    <?php
}
?>
</script>

<!-- Put this div tag to the place, where the Comments block will be -->

<div id="vk_comments" style="padding-top:20px;padding-left:230px;"></div>

<script type="text/javascript">
    VK.Widgets.Comments("vk_comments", {limit: 20, width: "500", attach: "*"});
</script>