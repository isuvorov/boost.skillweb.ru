<?php
$name = 'Рейитинг Пользователей';
$this->pageTitle = Yii::app()->name . ' - ' . $name;
$this->breadcrumbs = array(
    $name,
);
?>

<h1><?php echo $name; ?></h1>
<table class="table table-bordered table-striped">
    <tr>
        <th>
            ID
        </th>
        <th>
            Имя
        </th>
        <th>
            Количество побед
        </th>
        <th>
            Количество участий
        </th>
    </tr>
    <?php
    foreach ($users as $user) {
        ?>
        <tr>
            <td>
                <?php echo $user->id; ?>
            </td>
            <td>
                <?php echo $user->getName(); ?>
            </td>
            <td>
                <?php echo $user->getWinCount(); ?>
            </td>
            <td>
                <?php echo $user->getPlayCount(); ?>
            </td>
        </tr>
        <?php
    }
    ?>
</table>



