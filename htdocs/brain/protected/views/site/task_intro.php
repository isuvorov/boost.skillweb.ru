<?php $this->renderPartial('_task_head', $params); ?>
<?php
echo $game->getIntro();
?>
<br />
<center>
    <a href="<?php echo $this->createAbsoluteUrl('site/task', array('id' => $task->id)); ?>" class="btn btn-success btn-xlarge">
        <?php
        if ($user_task->isStarted()) {
            ?>
            Продолжить
            <?php
        } else {
            ?>
            Начать
            <?php
        }
        ?>
    </a>
</center>