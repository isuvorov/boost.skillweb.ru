<?php
$this->pageTitle = Yii::app()->name . ' - Войти';
$this->breadcrumbs = array(
    'Войти',
);
?>

<?php
if (isset($_GET['admin'])) {
    ?>
    <div class="form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableAjaxValidation' => true,
        ));
        ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'username'); ?>
            <?php echo $form->textField($model, 'username'); ?>
            <?php echo $form->error($model, 'username'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password'); ?>
            <?php echo $form->error($model, 'password'); ?>
            <p class="hint">
                Hint: You may login with <tt>demo/demo</tt>.
            </p>
        </div>

        <div class="row rememberMe">
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
            <?php echo $form->label($model, 'rememberMe'); ?>
            <?php echo $form->error($model, 'rememberMe'); ?>
        </div>

        <div class="row submit">
            <?php echo CHtml::submitButton('Login'); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div><!-- form -->
    <?php
}
?>
<h2>Войти с помощью соц сетей:</h2>
<?php Yii::app()->eauth->renderWidget(); ?>