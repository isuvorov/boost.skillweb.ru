<?php
$name = 'Рейтинг Пользователей';
$this->pageTitle = Yii::app()->name . ' - ' . $name;
$this->breadcrumbs = array(
    $name,
);

?>

<h1><?php echo $name; ?></h1>
<table class="table table-bordered table-striped">
    <tr>
        <th>
            Имя
        </th>
        <th>
            Участий
        </th>
        <th>
            Побед
        </th>
        <th>
            Очков
        </th>
    </tr>
    <?php
    foreach ($users as $user) {
        if(!Yii::app()->user->getModel()->isTester() && $user->isTester()){
            continue;
        }
        ?>
        <tr>
            <td>
                <a href="<?php echo $user->getProfileLink(); ?>"><?php echo $user->getName(); ?></a><br>
                <?php echo $user->created_at; ?>
            </td>
            <td>
                <?php echo $user->getTasksCount(); ?>
            </td>
            <td>
                <?php echo $user->getFinishedTasksCount(); ?>
            </td>
            <td>
                <?php echo $user->getScore(); ?>
            </td>
        </tr>
        <?php
    }
    ?>
</table>