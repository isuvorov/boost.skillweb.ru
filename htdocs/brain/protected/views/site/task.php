<?php $this->renderPartial('_task_head', $params); ?>
<?php
if ($_GET['do'] == 'error') {
    ?>

    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <p><strong>ОШИБКА!</strong> 
            Ответ неверный, введите другой ответ или пропустите текущий уровень. (к данному уровню можно будеть вернуться позднее)<br>
        <p>
            <a href="<?php echo $this->createAbsoluteUrl('site/task', array('id' => $task->id, 'do' => 'intro')); ?>" class="btn btn-info">Прочитать задание</a>
            <a href="<?php echo $this->createAbsoluteUrl('site/task', array('id' => $task->id, 'do' => 'skip')); ?>" class="btn btn-warning">Отложить задачу</a>
    </div>
    <?php
}
?>

<?php
/*
  $max_number = 64;
  function number_to_color($number) {
  $number = ($number & 3) << 6 | ($number & 3 << 2) << 12 | ($number & 3 << 4) << 18;
  return hex_to_color($number);
  }

  function hex_to_color($n) {
  return(substr("000000" . dechex($n), -6));
  }

  for ($number = 0; $number < $max_number; $number++) {
  $color = number_to_color($number);
  ?>
  <span style="background:#<?php echo $color; ?>;"><?php echo $color; ?></span>
  <?php
  } */
?>
<div class="level">
    <?php $user_task->game->drawLevel(); ?>
</div>
<div class="score">
    <table class="table table-bordered table-striped">
        <tr>
            <th>
                Имя
            </th>
            <td>
                <?php echo $user->getName(); ?>
            </td>
        </tr>

        <?php
        if ($user_task->isFinished()) {
            ?>
            <tr>
                <th>
                    Статус
                </th>
                <td>
                    Завершена, очки подсчитаны
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <th>
                Время игры
            </th>
            <td>
                <?php
                if ($user_task->isFinished()) {
                    ?>
                    <?php echo $user_task->getPlayDuration(); ?><br>
                    <?php echo $user_task->getPlayStart(); ?><br>
                    <?php echo $user_task->getPlayFinish(); ?>
                    <?php
                } else {
                    ?>
                    <span id="count_up">
                        <?php echo $user_task->getPlayDuration(); ?>
                    </span><br>
                    <?php
                }
                ?>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
                <script type="text/javascript" src="http://keith-wood.name/js/jquery.countdown.js"></script>
                <script>
                    $('#count_up').countdown({since: new Date('<?php echo $user_task->getPlayStart(); ?>'), compact: true,
                        layout: '{hnn}{sep}{mnn}{sep}{snn}', description: ''});
                </script>
            </td>
        </tr>
        <tr>
            <th>
                Уровень  <?php echo $user_task->game->getLevelId() + 1; ?>
            </th>
            <td>
                <?php
                foreach ($user_task->game->getLevelsStatus() as $id => $status) {
                    switch ($status) {
                        case 'f':
                            $class = 'badge-success"';
                            break;
                        case 'c':
                            $class = 'badge-important';
                            break;
                        case 's':
                            $class = 'badge-warning';
                            break;
                        default:
                            $class = '';
                            break;
                    }
                    if ($id == $game->getLevelId()) {
                        $class = 'badge-info';
                    }
                    $badge = '<span class="badge ' . $class . '">' . ($id + 1) . '</span>';
                    if ($user->isAdmin() || $class == 'badge-warning') {
                        ?>
                        <a href="<?php echo $this->createAbsoluteUrl('site/task', array('id' => $task->id, 'level' => $id)); ?>"><?php echo $badge; ?></a>
                        <?php
                    } else {
                        echo $badge;
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <th>
                Ошибок
            </th>
            <td>
                <?php echo $user_task->game->getErrors(); ?>
            </td>
        </tr>
        <tr>
            <th>
                Очков (за эту задачу / всего)
            </th>
            <td>
                <?php echo $user_task->game->getScore(); ?> / 
                <?php echo $user->getScore(); ?>
            </td>
        </tr>
        <?php
        if ($user->isAdmin()) {
            ?>
            <th>
                Ответ на задачу
            </th>
            <td>
                <?php echo $game->getLevelAnswer(); ?>
            </td>
            <?php
        }
        ?>
        <?php
        if ($game->isCanFinish()) {
            ?>

            <tr>
                <td colspan="2">

                    <a href="<?php echo $this->createAbsoluteUrl('site/task', array('id' => $task->id, 'do' => 'finish')); ?>" class="btn btn-info">Зафиксировать результат и выйти</a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>

</div>
<form action="" method="POST">
    <?php $user_task->game->drawAnswerForm(); ?>

    <p>

        <a href="<?php echo $this->createAbsoluteUrl('site/task', array('id' => $task->id, 'do' => 'skip')); ?>" class="btn btn-warning">Отложить задачу</a> или 
        <input type="submit" class="btn btn-success" value="Ответить">
    </p>
</form>