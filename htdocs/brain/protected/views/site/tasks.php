<?php
$name = 'Задачи';
$this->pageTitle = Yii::app()->name . ' - '.$name;
$this->breadcrumbs = array(
    $name,
);
?>
<h1><?php echo $name; ?></h1>
<table class="table table-bordered table-striped">
        <tr>
            <th>
              Название
            </th>
            <th>
                Начало
            </th>
            <th>
                Участников
            </th>
            <th>
                Финалистов
            </th>
        </tr>
    <?php
    
    foreach ($tasks as $task) {
        
        ?>
        <tr>
            <td>
                
                <a href="<?php echo $this->createAbsoluteUrl('site/task', array('id' => $task->id, 'do' => 'intro')); ?>"> <?php echo $task->name; ?></a>
            </td>
            <td>
                <?php echo date("Y.m.d H:i:s", strtotime($task->started_at)); ?>
            </td>
            <td>
               <?php echo $task->getUsersCount(); ?>
            </td>
            <td>
               <?php echo $task->getUsersFinalCount(); ?>
            </td>
        </tr>
        <?php
    }
    ?>
</table>