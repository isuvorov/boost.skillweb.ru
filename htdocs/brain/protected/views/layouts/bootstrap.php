<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>SkillMe</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 20px;
                padding-bottom: 40px;
            }

            /* Custom container */
            .container-narrow {
                margin: 0 auto;
                max-width: 1000px;
            }
            .container-narrow > hr {
                margin: 30px 0;
            }

            /* Main marketing message and sign up button */
            .jumbotron {
                margin: 60px 0;
                text-align: center;
            }
            .jumbotron h1 {
                font-size: 72px;
                line-height: 1;
            }
            .jumbotron .btn {
                font-size: 21px;
                padding: 14px 24px;
            }

            /* Supporting marketing content */
            .marketing {
                /*margin: 60px 0;*/
            }
            .marketing p + h4 {
                margin-top: 28px;
            }
        </style>
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="/brain/css/bootstrap_update.css" rel="stylesheet">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="/bootstrap/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <?php /*
          <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/bootstrap/ico/apple-touch-icon-144-precomposed.png">
          <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/bootstrap/ico/apple-touch-icon-114-precomposed.png">
          <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/bootstrap/ico/apple-touch-icon-72-precomposed.png">
          <link rel="apple-touch-icon-precomposed" href="/bootstrap/ico/apple-touch-icon-57-precomposed.png">
          <link rel="shortcut icon" href="/bootstrap/ico/favicon.png">

         */ ?>
    </head>

    <body>

        <div class="container-narrow">

            <div class="masthead">


                <div id="mainmenu"  class="nav nav-pills pull-right">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items' => array(
                            array('label' => 'Главная', 'url' => array('/')),
                            array('label' => 'О сайте', 'url' => array('/site/about')),
                            array('label' => 'Соревнования', 'url' => array('site/tasks')),
                            array('label' => 'Пользователи', 'url' => array('site/users')),
                            array('label' => 'Войти', 'url' => array('site/login'), 'visible' => Yii::app()->user->isGuest),
                            array('label' => 'Выйти (' . Yii::app()->user->name . ')', 'url' => array('site/logout'), 'visible' => !Yii::app()->user->isGuest)
                        ),
                        'htmlOptions' => array(
                            'class' => 'nav nav-pills pull-right'
                        )
                    ));
                    ?>
                </div><!-- mainmenu -->
                <h3 class="muted"><span class="color_orange">Skill</span>Me.Brain<sup><small><span class="label label-warning">BETA</span></small></sup> </h3> 
            </div>

            <div class="content">


                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                    'homeLink' => CHtml::link('Главная', Yii::app()->homeUrl),
                ));
                ?><!-- breadcrumbs -->
                <?php echo $content; ?>

            </div>


            <hr>
            <div class="footer">

                <div class="row-fluid">
                    <div class="span5">
                        &copy; 
                        <a href="http://skillclub.ru">SkillClub</a>, 
                        <a href="http://skillme.in">SkillMe</a>,
                        <a href="http://skillme.in/brain"><img src="/brain/brain_logo.png" width="24"/></a>
                        <?php // ,<a href="http://isuvorov.ru">Igor Suvorov</a>   ?>
                        2013
                    </div>
                    <div class="span5">
                        <script type="text/javascript">
                            VK.init({apiId: 3851786, onlyWidgets: true});
                        </script>

                        <!-- Put this div tag to the place, where the Like block will be -->
                        <div id="vk_like"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like", {type: "button", verb: 1});
                        </script>
                    </div>
                    <div class="span2">
                        <a href="#top">Наверх</a>
                    </div>
                </div>
            </div>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/bootstrap/js/jquery.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <!--
        <script src="/bootstrap/js/jquery.js"></script>
        <script src="/bootstrap/js/bootstrap-transition.js"></script>
        <script src="/bootstrap/js/bootstrap-alert.js"></script>
        <script src="/bootstrap/js/bootstrap-modal.js"></script>
        <script src="/bootstrap/js/bootstrap-dropdown.js"></script>
        <script src="/bootstrap/js/bootstrap-scrollspy.js"></script>
        <script src="/bootstrap/js/bootstrap-tab.js"></script>
        <script src="/bootstrap/js/bootstrap-tooltip.js"></script>
        <script src="/bootstrap/js/bootstrap-popover.js"></script>
        <script src="/bootstrap/js/bootstrap-button.js"></script>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>
        <script src="/bootstrap/js/bootstrap-carousel.js"></script>
        <script src="/bootstrap/js/bootstrap-typeahead.js"></script>
        -->
        <script src="/script.js"></script>

        <div id="signup" class="modal hide fade in" style="display: none; ">
            <div class="modal-header">
                <a class="close" data-dismiss="modal">×</a>
                <h3>Запишись к нам</h3>
            </div>
            <div class="modal-body">
                <form name="register" class="form-horizontal">
                    <fieldset>


                        <!-- Prepended text-->
                        <div class="control-group">
                            <label class="control-label">Ваше Имя</label>
                            <div class="controls">
                                <div class="input">
                                    <input name="signup[name]" id="prependedtext" name="prependedtext" class="span3" placeholder="Имя" type="text" required />
                                </div>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Ваш Email</label>
                            <div class="controls">
                                <div class="input">
                                    <input name="signup[email]" id="prependedtext" name="prependedtext" class="span3" placeholder="Email" type="email" required />
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-success" onclick="signup();">Хочу, все знать!</a>
                <a href="#" class="btn" data-dismiss="modal">Нет, спасибо</a>
            </div>
        </div>



        <div id="write_review" class="modal hide fade in" style="display: none; ">
            <div class="modal-header">
                <a class="close" data-dismiss="modal">×</a>
                <h3>Запишись к нам</h3>
            </div>
            <div class="modal-body">
                <form name="register" class="form-horizontal">
                    <fieldset>


                        <!-- Prepended text-->
                        <div class="control-group">
                            <label class="control-label">Ваше Имя</label>
                            <div class="controls">
                                <div class="input">
                                    <input name="signup[name]" id="prependedtext" name="prependedtext" class="span3" placeholder="Имя, Компания, Должность" type="text" required />
                                </div>
                            </div>
                        </div>



                        <div class="control-group">
                            <label class="control-label">Ваш Email</label>
                            <div class="controls">
                                <div class="input">
                                    <input name="signup[email]" id="prependedtext" name="prependedtext" class="span3" placeholder="Email" type="email" required />
                                </div>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label">Ваш отзыв</label>
                            <div class="controls">
                                <div class="input">
                                    <textarea name="signup[review]" id="prependedtext" name="prependedtext" class="span3" placeholder="Отзыв в свободной форме" type="review" required style="height:150px;"></textarea>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-success" onclick="signup();">Отправить отзыв</a>
                <a href="#" class="btn" data-dismiss="modal">Не сейчас, спасибо</a>
            </div>
        </div>
        <!-- Yandex.Metrika counter --><script type="text/javascript">(function(d, w, c) {
                                (w[c] = w[c] || []).push(function() {
                                    try {
                                        w.yaCounter22194755 = new Ya.Metrika({id: 22194755, webvisor: true, clickmap: true, trackLinks: true, accurateTrackBounce: true});
                                    } catch (e) {
                                    }
                                });
                                var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
                                    n.parentNode.insertBefore(s, n);
                                };
                                s.type = "text/javascript";
                                s.async = true;
                                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
                                if (w.opera == "[object Opera]") {
                                    d.addEventListener("DOMContentLoaded", f, false);
                                } else {
                                    f();
                                }
                            })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/22194755" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    </body>
</html>
