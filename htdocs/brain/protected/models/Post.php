<?php

/**
 * This is the model class for table "{{post}}".
 *
 * The followings are the available columns in table '{{post}}':
 * @property string $id
 * @property string $post_id
 * @property string $user_name
 * @property string $user_id
 * @property string $user_avatar
 * @property string $goals
 * @property string $goals_html
 * @property string $post_link
 * @property string $post_date
 * @property string $vkcomments_link
 * @property string $day_number
 */
class Post extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{post}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('post_id, user_id, day_number', 'length', 'max'=>10),
			array('user_name', 'length', 'max'=>255),
			array('user_avatar, goals, goals_html, post_link, post_date, vkcomments_link', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, post_id, user_name, user_id, user_avatar, goals, goals_html, post_link, post_date, vkcomments_link, day_number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'post_id' => 'Post',
			'user_name' => 'User Name',
			'user_id' => 'User',
			'user_avatar' => 'User Avatar',
			'goals' => 'Goals',
			'goals_html' => 'Goals Html',
			'post_link' => 'Post Link',
			'post_date' => 'Post Date',
			'vkcomments_link' => 'Vkcomments Link',
			'day_number' => 'Day Number',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('post_id',$this->post_id,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('user_avatar',$this->user_avatar,true);
		$criteria->compare('goals',$this->goals,true);
		$criteria->compare('goals_html',$this->goals_html,true);
		$criteria->compare('post_link',$this->post_link,true);
		$criteria->compare('post_date',$this->post_date,true);
		$criteria->compare('vkcomments_link',$this->vkcomments_link,true);
		$criteria->compare('day_number',$this->day_number,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
