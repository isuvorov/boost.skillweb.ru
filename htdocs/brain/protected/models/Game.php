<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Game {

    protected $level_id = 0;
    protected $levels_status = array();
    protected $errors = 0;

    public function getErrors() {
        return $this->errors;
    }

    public function getLevelsStatus() {
        $statuses = array();
        foreach ($this->getLevels() as $level_id => $level_params) {
            $status = $this->levels_status[$level_id];
            if (!$status) {
                $status = '?';
            }
            $statuses[$level_id] = $status;
        }
        return $statuses;
    }

    public function isCanFinish() {
        return !in_array('?', $this->getLevelsStatus());
    }

    public function isAutoFinish() {
        return !in_array('?', $this->getLevelsStatus()) && !in_array('s', $this->getLevelsStatus());
    }

    public function getLevelId() {
        return $this->level_id;
    }

    public function getMaximumLevelId() {
        return count($this->getLevels());
    }

    public function setLevelStatus($status) {
        $level_id = $this->level_id;
        $this->levels_status[$level_id] = $status;
    }

    public function getLevelStatus() {
        $statuses = $this->getLevelsStatus();
        return $statuses[$this->level_id];
    }

    public function nextLevel() {
        for ($i = 0; $i < $this->getMaximumLevelId(); $i++) {
            $this->level_id = ($this->level_id + 1) % $this->getMaximumLevelId();
            if ($this->getLevelStatus() != 'f')
                return true;
        }
        return false;
    }

    public function winLevel() {
        $this->setLevelStatus('f');
        $this->nextLevel();
    }

    public function skipLevel() {
        $this->setLevelStatus('s');
        $this->nextLevel();
    }

    public function setLevel($level_id) {
        $this->level_id = ($level_id) % $this->getMaximumLevelId();
    }

    public function getLevelParams($level_id = null) {
        if ($level_id === null) {
            $level_id = $this->level_id;
        }
        if ($level_id === null) {
            $level_id = 0;
        }
        $levels = $this->getLevels();
        return $levels[$level_id];
    }

    public function applyAnswer($post) {
        $answer = $this->analyseAnswer($post);
        //var_dump($answer);
        //exit;
        if ($answer) {
            $this->winLevel();
        } else {
            $this->errors += 1;
        }
        return $answer;
    }

    public function getScore() {
        $score = $this->errors * -2;
        foreach ($this->getLevelsStatus() as $id => $status) {
            switch ($status) {
                case 'f':
                    $score += 10;
                    break;
                case 's':
                    $score += -10;
                    break;
                default:
                    break;
            }
        }
        return $score;
    }

    public function getLevelAnswer() {
        $level_params = $this->getLevelParams();
        $answer = $this->getAnswer($level_params);
        if (is_array($answer))
            return print_r($answer, 1);
        else
            return $answer;
    }

    public function getIntro() {
        return '';
    }

    public function getOutro() {
        return '';
    }

}
