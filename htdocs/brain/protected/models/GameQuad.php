<?php

class GameQuad extends Game {

    public static function getLevels() {
        $levels = array(
            array(1, 1),
            array(2, 2),
            array(3, 3),
            array(4, 4),
            array(1, 5),
            array(3, 5),
            array(4, 9),
            array(5, 7),
            array(33, 1),
            array(45, 85),
        );
        return $levels;
    }

    public function drawLevel() {
        $level_params = $this->getLevelParams();
        $max_pixel_size = 600;
        $max_size = max($level_params);
        $width = round($max_pixel_size / $max_size);
        $width = min(100, max(10, $width));
        ?>
        <style>
            .level table, .level  td{
                border:1px black solid;
                border-collapse:collapse;
                padding:0px;
                margin:0px;
            }
            .level{
                font-size:1px;
                line-height:1px;
                padding:10px;
            }
            
            


            .level  td{
                min-height:10px;
                min-width:10px;
                max-height:100px;
                max-width:100px;
                width: <?php echo $width; ?>px;
                height: <?php echo $width; ?>px;
            }
        </style>
        <table>
            <?php
            for ($i = 0; $i < $level_params[0]; $i++) {
                ?>
                <tr>
                    <?php
                    for ($j = 0; $j < $level_params[1]; $j++) {
                        ?>
                        <td>&nbsp;</td>
                        <?
                    }
                    ?>
                <tr>
                    <?php
                }
                ?>
        </table>
        <?php
    }

    public function getScore() {
        $score = $this->errors * -2;
        foreach ($this->getLevelsStatus() as $id => $status) {
            switch ($status) {
                case 'f':
                    $score += 10;
                    break;
                case 's':
                    $score += -10;
                    break;
                default:
                    break;
            }
        }
        return $score;
    }

    public function getAnswer($params) {
        $w = $params[0];
        $h = $params[1];
        $s = 0;
        for (; $w > 0 && $h > 0; $w--, $h--) {
            $s += $h * $w;
        }
        return $s;
    }

    public function drawAnswerForm() {
        ?>
        Ответ: <input name="count" type="text">
        <?php
    }

    public function analyseAnswer($post) {
        $level_params = $this->getLevelParams();
        $answer = $this->getAnswer($level_params);
        return $answer == $post['count'];
    }
    
    
    public function getIntro() {
        ?>
        <div>
            <p>Необходимо подсчитать колличество квадратов, которые показаны на экране
            </p>
            <h3>Пример:</h3>
            <img src="http://coder24.ru/screen/s64288c4db236.png" style="border:2px #eee solid"/>
        </div>
        <?php
    }
    
    
    public function getOutro() {
        ?>
        <div>
            <p>Вы великий маг раз смогли сосчитать столько квадратов</p>
        </div>
        <?php
    }

}
