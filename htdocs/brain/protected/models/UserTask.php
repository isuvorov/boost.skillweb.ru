<?php

/**
 * This is the model class for table "{{user_task}}".
 *
 * The followings are the available columns in table '{{user_task}}':
 * @property string $id
 * @property string $user_id
 * @property string $task_id
 * @property string $started_at
 * @property string $finished_at
 * @property string $updated_at
 * @property string $score
 * @property string $info
 */
class UserTask extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{user_task}}';
    }

    public function behaviors() {
        return array(
            'userTaskBehavior' => array(
                'class' => 'UserTaskBehavior',
            ),
        );
    }
    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, task_id, started_at, score', 'required'),
            array('user_id, task_id, score', 'length', 'max' => 10),
            array('finished_at, updated_at', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, task_id, started_at, finished_at, updated_at, score, info', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'task_id' => 'Task',
            'started_at' => 'Started At',
            'finished_at' => 'Finished At',
            'updated_at' => 'Updated At',
            'score' => 'Score',
            'info' => 'Info',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('task_id', $this->task_id, true);
        $criteria->compare('started_at', $this->started_at, true);
        $criteria->compare('finished_at', $this->finished_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('score', $this->score, true);
        $criteria->compare('info', $this->info, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserTask the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
