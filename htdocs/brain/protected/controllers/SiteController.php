<?php

class SiteController extends Controller {

    public $layout = 'column1';

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('*'),
                'roles' => array('@'),
            ),
            array('deny',
                'actions' => array('task', 'users'),
                'users' => array('?'),
            ),
        );
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionIndex() {
        $this->actionTasks();
    }

    public function genMonth() {
        $rand = rand(1, 12);
        if ($rand < 10) {
            $rand = '0' . $rand;
        }
        return $rand;
    }

    public function actionGen() {

        $months = array('22', '02', '20');
        for ($i = 0; $i < 100; $i++) {
            $month = $months[rand(0, count($months) - 1)];
            $day = $months[rand(0, count($months) - 1)];
            $year1 = $months[rand(0, count($months) - 1)];
            $year2 = $months[rand(0, count($months) - 1)];
            echo "$month-$month-$year1";
        }
        /*
          for ($i = 0; $i < 100; $i++) {
          echo '-29-02-' . rand(2014, 2033);
          } */
    }

    public $name = 'game_name';

    public function actionTask($id) {
        $user = Yii::app()->user->getModel();
        $user_id = Yii::app()->user->id;
        $task = Task::model()->findByPk($id);
        $user_task = $task->getUserTask($user_id);
        $game = $user_task->getGame();
        $this->name = $task->name;

        $params = array(
            'task' => $task,
            'user_task' => $user_task,
            'user' => $user,
            'game' => $game,
        );

        if (!$user->isTester() && !$task->isStarted()) {
            $this->render('task_not_started', $params);
            return 0;
            throw new Exception('Соревнование начнется ' . date("Y.m.d H:i:s", strtotime($task->started_at)));
        }


        if ($_GET['do'] == 'intro') {
            $this->render('task_intro', $params);
            return 0;
        }

        if ($_GET['do'] == 'outro' && $user_task->isFinished()) {
            $this->render('task_outro', $params);
            return 0;
        }


        if (isset($_GET['level'])) {
            $user_task->game->setLevel($_GET['level']);
            $user_task->saveGame();
            $user_task->updated();
            $user_task->save();
            $this->redirect($this->createAbsoluteUrl('site/task', array('id' => $id)));
            return 0;
        }
        //var_dump($_POST);
        if ($_GET['do'] == 'skip') {
            $user_task->game->skipLevel();
            $user_task->saveGame();
            $user_task->updated();
            $user_task->save();
            $this->redirect($this->createAbsoluteUrl('site/task', array('id' => $id)));
            return 0;
        }
        if ($_GET['do'] == 'finish' || $game->isAutoFinish()) {
            if ($user_task->isFinished()) {
                $this->redirect($this->createAbsoluteUrl('site/task', array('id' => $task->id, 'do' => 'outro')));
                return 0;
            }
            if ($game->isCanFinish()) {
                $user_task->finish();
                $user_task->updated();
                $user_task->save();
                $user->score = $user->getFinishedTasksCount();
                $user->save();
                $this->redirect($this->createAbsoluteUrl('site/task', array('id' => $task->id, 'do' => 'outro')));
                return 0;
            }
        }
        if ($_POST) {
            $answer = $user_task->game->applyAnswer($_POST);
            $user_task->saveGame();
            $user_task->updated();
            $user_task->save();
            /* var_dump($user_task);
              var_dump($user_task->game); */
            if ($answer) {
                $this->redirect($this->createAbsoluteUrl('site/task', array('id' => $id)));
            } else {
                $this->redirect($this->createAbsoluteUrl('site/task', array('id' => $id, 'do' => 'error')));
            }
            return 0;
        }


        $this->render('task', $params);
    }

    public function actionUser() {
        $this->actionUsers();
    }

    public function actionUsers() {
        $users = User::model()->findAll();
        $this->render('users', array('users' => $users));
    }

    public function actionabout() {
        $this->render('about');
    }

    /**
     * Displays the contact page
     */
    public function actionTasks() {
        $tasks = Task::model()->findAll();
        $this->render('tasks', array('tasks' => $tasks));
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
                mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLoginOld() {
        if (!defined('CRYPT_BLOWFISH') || !CRYPT_BLOWFISH)
            throw new CHttpException(500, "This application requires that PHP was compiled with Blowfish support for crypt().");

        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionLogin() {
        $service = Yii::app()->request->getQuery('service');
        if (isset($service)) {
            $authIdentity = Yii::app()->eauth->getIdentity($service);
            $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
            $authIdentity->cancelUrl = $this->createAbsoluteUrl('site/login');

            if ($authIdentity->authenticate()) {
                $identity = new ServiceUserIdentity($authIdentity);


                /*
                 * 
                  if($this->profile && $profile = unserialize($this->profile)){
                  $this->setState('name', $profile['name']);
                  }
                 * 
                 */

                // Успешный вход
                //var_dump($identity);
                if ($identity->authenticate()) {
                    $duration = 3600 * 24 * 30;
                    //var_dump($identity->authenticate());
                    //var_dump($identity->authenticate());
                    $ans = Yii::app()->user->login($identity);
                    //var_dump($ans);


                    $identity = new UserIdentity($identity->username, md5($identity->username));
                    $identity->authenticate();
                    $duration = 3600 * 24 * 30; // 30 days
                    $ans = Yii::app()->user->login($identity, $duration);
                    //var_dump($ans);
                    //exit;
                    /* Yii::app()->user->login($identity, 3600*24*30);
                      var_dump( Yii::app()->user); */
                    // Специальный редирект с закрытием popup окна
                    $authIdentity->redirect();
                } else {
                    // Закрываем popup окно и перенаправляем на cancelUrl
                    $authIdentity->cancel();
                }
            }

            // Что-то пошло не так, перенаправляем на страницу входа
            $this->redirect(array('site/login'));
        }
        if (!defined('CRYPT_BLOWFISH') || !CRYPT_BLOWFISH)
            throw new CHttpException(500, "This application requires that PHP was compiled with Blowfish support for crypt().");

        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

}
