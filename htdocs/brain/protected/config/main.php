<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Yii Blog Demo',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.services.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'qwe123qwe',
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    'defaultController' => 'site',
    // application components
    'components' => array(
        'loid' => array(
            'class' => 'application.extensions.lightopenid.loid',
        //'class' => 'ext.lightopenid.LightOpenID',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'services' => array(// You can change the providers and their classes.
                /* 'google' => array(
                  'class' => 'GoogleOpenIDService',
                  ),
                  'yandex' => array(
                  'class' => 'YandexOpenIDService',
                  ),
                  'twitter' => array(
                  'class' => 'TwitterOAuthService',
                  'key' => '...',
                  'secret' => '...',
                  ),
                  'facebook' => array(
                  'class' => 'FacebookOAuthService',
                  'client_id' => '...',
                  'client_secret' => '...',
                  ), */
                'vkontakte' => array(
                    'class' => 'VKontakteOAuthService',
                    //'client_id' => '4003301',
                    //'client_secret' => 'teC3LozewJxgnk2Ud0P8',
                    'client_id' => '4003279',
                    'client_secret' => 'eE8juiRFI5pykiVWRaB5',
                ),
            /* 'mailru' => array(
              'class' => 'MailruOAuthService',
              'client_id' => '...',
              'client_secret' => '...',
              ), */
            ),
        ),
        'user' => array(
// enable cookie-based authentication
            'allowAutoLogin' => true,
            'class' => 'WebUser',
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=skillme',
            'emulatePrepare' => true,
            'username' => 'skillme',
            'password' => 'tdd8HxVqXZmk',
            'charset' => 'utf8',
            'tablePrefix' => 'brain_',
            'enableParamLogging' => true
        ),
        /* 'db'=>array(
          'connectionString' => 'sqlite:protected/data/blog.db',
          'tablePrefix' => 'tbl_',
          ), */
// uncomment the following to use a MySQL database
        /*
          'db'=>array(
          'connectionString' => 'mysql:host=localhost;dbname=blog',
          'emulatePrepare' => true,
          'username' => 'root',
          'password' => '',
          'charset' => 'utf8',
          'tablePrefix' => 'tbl_',
          ),
         */
        'errorHandler' => array(
// use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<id:\d+>' => 'site/task',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            ),
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
            /*    array(
                    'class' => 'CWebLogRoute',
                    //'categories' => 'application',
                    'levels' => 'error, warning, trace, info',
                //'levels'=>'error, warning, trace, profile, info',
                ), /**/
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'trace',
                    'categories' => 'system.db.*',
                    'logFile' => 'sql.log',
                ),
                array(
                    'class' => 'CEmailLogRoute',
                    'levels' => 'error, warning',
                    'emails' => 'skillme.brain@coder24.ru',
                ),
             /*   array(
// направляем результаты профайлинга в ProfileLogRoute (отображается
// внизу страницы)
                    'class' => 'CProfileLogRoute',
                    'levels' => 'profile',
                    'enabled' => true,
                ),*/
            ),
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => require(dirname(__FILE__) . '/params.php'),
);
