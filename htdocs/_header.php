<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>SkillMe</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 20px;
                padding-bottom: 40px;
            }

            /* Custom container */
            .container-narrow {
                margin: 0 auto;
                max-width: 1000px;
            }
            .container-narrow > hr {
                margin: 30px 0;
            }

            /* Main marketing message and sign up button */
            .jumbotron {
                margin: 60px 0;
                text-align: center;
            }
            .jumbotron h1 {
                font-size: 72px;
                line-height: 1;
            }
            .jumbotron .btn {
                font-size: 21px;
                padding: 14px 24px;
            }

            /* Supporting marketing content */
            .marketing {
                /*margin: 60px 0;*/
            }
            .marketing p + h4 {
                margin-top: 28px;
            }
        </style>
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="/bootstrap/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <?php
        /*
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/bootstrap/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/bootstrap/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/bootstrap/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/bootstrap/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="/bootstrap/ico/favicon.png">
         
         */?>
    </head>

    <body>


        <style type="text/css">
            body {
                padding-top: 20px;
                padding-bottom: 60px;
            }

            /* Custom container */
            .container {
                margin: 0 auto;
                max-width: 1000px;
            }
            .container > hr {
                margin: 60px 0;
            }

            /* Main marketing message and sign up button */
            .jumbotron {
                margin: 80px 0;
                text-align: center;
            }
            .jumbotron h1 {
                font-size: 100px;
                line-height: 1;
            }
            .jumbotron .lead {
                font-size: 24px;
                line-height: 1.25;
            }
            .jumbotron .btn {
                font-size: 21px;
                padding: 14px 24px;
            }

            /* Supporting marketing content */
            .marketing {
                /*margin: 60px 0;*/
            }
            .marketing p + h4 {
                margin-top: 28px;
            }


            /* Customize the navbar links to be fill the entire space of the .navbar */
            .navbar .navbar-inner {
                padding: 0;
            }
            .navbar .nav {
                margin: 0;
                display: table;
                width: 100%;
            }
            .navbar .nav li {
                display: table-cell;
                width: 1%;
                float: none;
            }
            .navbar .nav li a {
                font-weight: bold;
                text-align: center;
                border-left: 1px solid rgba(255,255,255,.75);
                border-right: 1px solid rgba(0,0,0,.1);
            }
            .navbar .nav li:first-child a {
                border-left: 0;
                border-radius: 3px 0 0 3px;
            }
            .navbar .nav li:last-child a {
                border-right: 0;
                border-radius: 0 3px 3px 0;
            }

            .color_orange{
                color: #f89406;
            }
        </style>



        <div class="container-narrow">

            <div class="masthead">

<?php /*
                <ul class="nav nav-pills pull-right">
                    <li class="2active"><a href="#">Главная</a></li>
                    <li><a href="/reviews.php">Отзывы</a></li>
                    <li><a href="/#contacts">Контакты</a></li>
                </ul>*/?>
                <h3 class="muted"><span class="color_orange">Skill</span>Me <sup><small><span class="label label-warning">BETA</span></small></sup> </h3> 
            </div>
        

        <?php /*
          <!-- Jumbotron -->
          <div class="jumbotron">
          <h1>Marketing stuff!</h1>
          <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <a class="btn btn-large btn-success" href="#">Get started today</a>
          </div>

          <hr>

          <!-- Example row of columns -->
          <div class="row-fluid">
          <div class="span4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
          </div>
          <div class="span4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
          </div>
          <div class="span4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.</p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
          </div>
          </div>

          <hr> */ ?>
        <style type ="text/css">
            .how_block {
                text-align: center;
            }

            .how_block p {
                text-indent: 10px;
                text-align: justify;
            }

            .without_margin {
                margin-top: 0px;
                margin-bottom: 0px;
            }

            .skillclub_text p {
                text-align: justify;
            }

            #reviews:after {
                position: absolute;
                top: 20px;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 100;
                background: url('https://4slona.com/i/transp_grad.png') repeat-x 0 100%;
                content: "";
            }

            #reviews .review_text {
                text-align: justify;
                margin-top: 10px;
                text-indent: 15px;
            }

            #reviews .review_author {
                display: table-row;  
            }

            #reviews .review_author .author_img {
                margin-right: 15px;
                width: 120px;
                height: 120px;
            }

            #reviews .review_author .author_info {
                text-align:center;
                vertical-align: middle;
                display:  table-cell;
            }

            #reviews .review_author .author_name {
                font-size: 16px;
                font-weight: bold;
            }

            #reviews .review_author .author_prof {
                font-size: 13px;
            }
            #reviews + #reviews {
                margin-top:40px;
            }

            .margin-left20 {
                margin-left: 20px;
            }

            .contacts {
                text-align: center;
            }
        </style>