<?php
if(!$_POST['signup']['email']){
    echo 0;
    exit;
}

$my_email = 'boost@coder24.ru';
$title = 'Boost.Skillweb';
$body = print_r($_POST['signup'], 1);
mail($my_email, $title, $body);



if(!$_POST['signup']['name']){
    $_POST['signup']['name'] = 'Участник';
}
$title = 'Мы записали вас. SkillWeb';
$body = 'и напишем вам в ближайшее время';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
#$headers .= 'To: Mary <mary@example.com>, Kelly <boost@skillweb.ru>' . "\r\n";
$headers .= 'From: Birthday Reminder <boost@skillweb.ru>' . "\r\n";
$headers .= 'Cc: boost@skillweb.ru' . "\r\n";
$headers .= 'Bcc: boost@skillweb.ru' . "\r\n";

$body = <<<STR

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Demystifying Email Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) { var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",mirage:{responsive:0,lazy:0},mirage2:{profile:false},oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/abv=2332175737/"},atok:"4fd04c193b11b4cf1cae412a205f385f",zone:"tutsplus.com",rocket:"a",apps:{}}];document.write('<script type="text/javascript" src="//ajax.cloudflare.com/cdn-cgi/nexp/abv=2310247235/cloudflare.min.js"><'+'\/script>')}}catch(e){};
//]]>
</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding: 0 0 30px 0;">
 
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
<tr>
<td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
<img style="display:none;visibility:hidden;" data-cfsrc="http://www.nightjar.com.au/tests/magic/images/h1.gif" alt="Creating Email Magic" width="300" height="230" data-cfstyle="display: block;"/><noscript><img src="http://www.nightjar.com.au/tests/magic/images/h1.gif" alt="Creating Email Magic" width="300" height="230" style="display: block;"/></noscript>
</td>
</tr>
<tr>
<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
<b>Lorem ipsum dolor sit amet!</b>
</td>
</tr>
<tr>
<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
</td>
</tr>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="260">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<img style="display:none;visibility:hidden;" data-cfsrc="http://www.nightjar.com.au/tests/magic/images/left.gif" alt="" width="100%" height="140" data-cfstyle="display: block;"/><noscript><img src="http://www.nightjar.com.au/tests/magic/images/left.gif" alt="" width="100%" height="140" style="display: block;"/></noscript>
</td>
</tr>
<tr>
<td style="padding: 25px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
</td>
</tr>
</table>
</td>
<td style="font-size: 0; line-height: 0;" width="20">
&nbsp;
</td>
<td width="260">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<img style="display:none;visibility:hidden;" data-cfsrc="http://www.nightjar.com.au/tests/magic/images/right.gif" alt="" width="100%" height="140" data-cfstyle="display: block;"/><noscript><img src="http://www.nightjar.com.au/tests/magic/images/right.gif" alt="" width="100%" height="140" style="display: block;"/></noscript>
</td>
</tr>
<tr>
<td style="padding: 25px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
&reg; Someone, somewhere 2013<br/>
<a href="#" style="color: #ffffff;"><font color="#ffffff">Unsubscribe</font></a> to this newsletter instantly
</td>
<td align="right" width="25%">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
<a href="http://www.twitter.com/" style="color: #ffffff;">
<img style="display:none;visibility:hidden;" data-cfsrc="http://www.nightjar.com.au/tests/magic/images/tw.gif" alt="Twitter" width="38" height="38" data-cfstyle="display: block;" border="0"/><noscript><img src="http://www.nightjar.com.au/tests/magic/images/tw.gif" alt="Twitter" width="38" height="38" style="display: block;" border="0"/></noscript>
</a>
</td>
<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
<a href="http://www.twitter.com/" style="color: #ffffff;">
<img style="display:none;visibility:hidden;" data-cfsrc="http://www.nightjar.com.au/tests/magic/images/fb.gif" alt="Facebook" width="38" height="38" data-cfstyle="display: block;" border="0"/><noscript><img src="http://www.nightjar.com.au/tests/magic/images/fb.gif" alt="Facebook" width="38" height="38" style="display: block;" border="0"/></noscript>
</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
 
</td>
</tr>
</table>
</body>
</html>
STR;

echo mail($_POST['signup']['email'], $title, $body, $headers) ? 1 : 0;
?>