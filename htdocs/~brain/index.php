<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();

function save() {
    $score = load();
    $score[$_SESSION['gamer_id']] = array(
        $_SESSION['gamer_name'],
        $_SESSION['level_id'],
        $_SESSION['wrong'],
        time()
    );
    file_put_contents('score.json', json_encode($score));
}

function load() {
    $score = json_decode(file_get_contents('score.json'), 1);
    return $score;
}

if (isset($_GET['reset'])) {
    unset($_SESSION['gamer_name']);
    unset($_SESSION['wrong']);
    unset($_SESSION['gamer_id']);
    unset($_SESSION['level_id']);
    header('location:?');
    exit;
}
if (isset($_GET['score'])) {
    $title = 'Рейтинговая таблица';
    ob_start();
    ?>
    <table class="table table-bordered table-striped">
        <tr>
            <th>
                Имя
            </th>
            <th>
                Уровень
            </th>
            <th>
                Количество ошибок
            </th>
            <th>
                Время решения
            </th>
        </tr>
        <?php
        foreach (load() as $time => $user) {
            ?>
            <tr>
                <?php
                foreach ($user as $i => $row) {
                    
                    if ($i == 3) {
                        $row = ceil(($row - $time) / 60) .' минут<br>' .date('Y-m-d H:i:s', $time).'<br>' .date('Y-m-d H:i:s', $row);
                        ?>
                        <td>
                            <?php echo $row; ?>
                        </td>
                        <?php
                    } else if($i == 1)  {
                        ?>
                        <td>
                            <?php echo $row == 10 ? 'Победа!' : $row; ?>
                        </td>
                        <?php
                    }  else {
                        ?>
                        <td>
                            <?php echo $row; ?>
                        </td>
                        <?php
                    }
                }
                ?>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
    $body = ob_get_contents();
    ob_end_clean();
    require_once './template.php';
    exit;
}
if ($_GET['level_up']) {
    $_SESSION['level_id'] += 1;
    header('location:?');
    exit;
}
if ($_POST['gamer_name']) {

    $_SESSION['gamer_name'] = $_POST['gamer_name'];
    $_SESSION['gamer_id'] = time();
    save();

    header('location:?');
    exit;
}
if (!$_SESSION['gamer_name']) {
    $title = 'Введите ваше имя';
    ob_start();
    ?>
    <form method="POST" >
        <input name="gamer_name">
        <input type="submit">
    </form>
    <?php
    $body = ob_get_contents();
    ob_end_clean();
    require_once './template.php';
    exit;
}

if ($_SESSION['name']) {
    ?>

    <?php
}

$level_id = $_SESSION['level_id'];
$levels = array(
    array(1, 1, 62135),
    array(2, 2, 27490),
    array(3, 3, 62580),
    array(4, 4, 10914),
    array(1, 5, 80232),
    array(3, 5, 17059),
    array(4, 9, 92972),
    array(5, 7, 73826),
    array(33, 1, 123123),
    array(45, 85, 77694),
);
/*
  foreach($levels as $level){
  echo rand() % 100000 . '<br>';
  } */

if ($_SESSION['level_id'] >= 10) {

    save();
    $title = 'Поздравляю, вы достигли финала!';
    ob_start();
    ?>
    <h2>Сообщите ваш код: 7279572795 <a href="http://vk.com/igor.suvorov">мне</a></h2>
    <?php
    $body = ob_get_contents();
    ob_end_clean();
    require_once './template.php';
    exit;
    ?>
    <?php
}
if (!$levels[$level_id]) {
    $level_id = 0;
}

$level = $levels[$level_id];

//$size = array(5, 7);
function count_square($w, $h) {
    if ($h < $w) {
        return count_square($h, $w);
    }
    if ($w <= 0 || $h <= 0) {
        return 0;
    }
    return $w * $h + count_square($h - 1, $w - 1);
}

if (!$_SESSION['wrong']) {
    $_SESSION['wrong'] = 0;
}
$title = 'Сколько здесь квадратов?';
if (isset($_POST['count'])) {
//var_dump(count_square($level[0], $level[1]));
//var_dump($_POST['count']);
    //echo count_square($level[0], $level[1]);
    if ($_POST['count'] == count_square($level[0], $level[1])) {


        $_SESSION['level_id'] = $level_id + 1;
        save();

        header('Location:?');
    } else {

        ob_start();
        $_SESSION['wrong'] += 1;
        ?>
        <h2>Ответ неверный</h2>
        <a href="?">Попробовать еще?</a>
        <?php
        $body = ob_get_contents();
        ob_end_clean();

        save();
        require_once './template.php';
    }

    exit;
}

ob_start();
require_once './level.php';
$body = ob_get_contents();
ob_end_clean();
require_once './template.php';
?>