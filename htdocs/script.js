$(function() {

});

function signup() {
    var error = false;
    var popup = $('#signup');
    var form = popup.find('form[name=register]');

    if ($.trim(form.find('input[name="signup[name]"]').val()) == 0) {
        error = true;
        form.find('input[name="signup[name]"]').parents('.control-group').addClass('error');
        form.find('input[name="signup[name]"]').parents('.control-group').removeClass('success');
    } else {
        form.find('input[name="signup[name]"]').parents('.control-group').addClass('success');
    }

    if (!checkEmail($.trim(form.find('input[name="signup[email]"]').val()))) {
        error = true;
        form.find('input[name="signup[email]"]').parents('.control-group').addClass('error');
        form.find('input[name="signup[email]"]').parents('.control-group').removeClass('success');
    } else {
        form.find('input[name="signup[email]"]').parents('.control-group').addClass('success');
    }

    if (!error) {
        form.find('.control-group').each(function() {
            $(this).removeClass('error');
            $(this).addClass('success');
        });

        $.ajax({
            type: 'POST',
            url: '/register.php',
            data: form.serialize(),
            success: function(data) {
                popup.find('.modal-footer').remove();
                popup.find('.modal-body').html('Поздравляем, теперь ты с нами!');
            }
        });

        //success срабатывает поздно, поэтому пока так
        setTimeout(function() {
            popup.find('.modal-footer').remove();
            popup.find('.modal-body').html('Поздравляем, теперь ты с нами!');
        }, 1000);

    }

    return false;
}

function checkEmail(value) {
    reg = /[а-яА-ЯA-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[а-яА-ЯA-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[а-яА-ЯA-Za-z0-9](?:[а-яА-ЯA-Za-z0-9-]*[а-яА-ЯA-Za-z0-9])?.)+[а-яА-ЯA-Za-z0-9](?:[а-яА-ЯA-Za-z0-9-]*[а-яА-ЯA-Za-z0-9])?/;
    if (!value.match(reg)) {
        console.log(1);
        return false;
    }

    return true;
}
